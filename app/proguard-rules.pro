# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\studio\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

#保留native的方法的方法名和包含native的类的类名不混淆
-keepclasseswithmembernames class *{
    native<methods>;
}

# 保留我们自定义控件（继承自View）中的set 和get不被混淆
-keepclassmembers pulice class * extends android.view.View{
    void set*(***);
    *** get*();
}
# 保留在Activity中的方法参数是view的方法，
# 这样以来我们在layout中写的onClick就不会被影响
-keepclassmembers public class * extends android.app.Activity{
    public void *(android.view.View);
}

#保留实现了Parcelable接口的类的类名以及Parcelable序列化类
-keep class * implements android.os.Parcelable{
    public static final android.os.Parcelable$Creator *;
}

#保留R$*类中静态变量字段
-keepclassmembers class **.R$*{
   public static <fields>;
}

-assumenosideeffects class android.util.Log{
   public static boolean isLoggable(java.lang.String ,int);
   public static int v(...);
   public static int i(...);
   public static int w(...);
   public static int d(...);
   public static int e(...);
}

-assumenosideeffects class com.example.log.Logger{
   public static int v(...);
   public static int i(...);
   public static int w(...);
   public static int d(...);
   public static int e(...);
}
# glide 的混淆代码
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
      **[] $VALUES;
      public *;
    }
-keep class com.bumptech.glide.integration.okhttp.OkHttpGlideModule
# banner 的混淆代码
-keep class com.youth.banner.** {
    *;
 }
 #Resource资源
 -dontwarn android.content.res.**