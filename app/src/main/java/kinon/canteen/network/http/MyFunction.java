package kinon.canteen.network.http;

import android.util.Log;

import io.reactivex.functions.Function;
import kinon.canteen.bean.base.BaseBean;

/**
 * Created by lhqq on 2018-01-11.
 */

public class   MyFunction<T> implements Function<BaseBean<T>,T> {
    @Override
    public T apply(BaseBean<T> tBaseBean) throws Exception {
        Log.e("lh", "MyFunction== "+tBaseBean.getCode() );
        if(!tBaseBean.getCode()){
            throw new ApiException(1001,tBaseBean.getMsg());
        }
        return tBaseBean.getData();

    }
}
