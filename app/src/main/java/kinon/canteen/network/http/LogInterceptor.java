package kinon.canteen.network.http;

import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by lhqq on 2017-12-19.
 */

public class LogInterceptor implements Interceptor {

    private String TAG="lh_http";

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Log.i(TAG, "okhttp3:" + request.toString());//输出请求前整个url
        long t1 = System.nanoTime();
        okhttp3.Response response = chain.proceed(chain.request());
        long t2 = System.nanoTime();
//        Log.v(TAG,response.request().url().toString()+response.headers());//输出一个请求的网络信息
        okhttp3.MediaType mediaType = response.body().contentType();
        String content = response.body().string();
        Log.i(TAG, "response body:" + content);//输出返回信息
        return response.newBuilder()
                .body(okhttp3.ResponseBody.create(mediaType, content))
                .build();
    }
}
