package kinon.canteen.network.http;

import org.json.JSONObject;

import java.util.ArrayList;

import io.reactivex.Flowable;
import kinon.canteen.bean.ApiBean;
import kinon.canteen.bean.ConfirmOrderBean;
import kinon.canteen.bean.HomeMsgBean;
import kinon.canteen.bean.SendCodeBean;
import kinon.canteen.bean.ShopCartBean;
import kinon.canteen.bean.UpCartBean;
import kinon.canteen.bean.GoodsBean;
import kinon.canteen.bean.HomeBean;
import kinon.canteen.bean.OrderQRCodeBean;
import kinon.canteen.bean.OrderTypeBean;
import kinon.canteen.bean.UserBean;
import kinon.canteen.bean.base.BaseBean;
import kinon.canteen.bean.order.DetailBean;
import kinon.canteen.bean.order.OrderBean;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by luohao on 2017-07-04.
 */

public interface RequestApi {


    /**
     * 根据手机号获取api地址
     * @param phone
     * @return
     */
    @FormUrlEncoded
    @POST("app/getAPI")
    Call<String> getApi(@Field("phone") String phone);

    /**
     * 登录
     * @param phone
     * @param password
     * @param type 手机类型 2安卓 3IOS
     * @param model
     * @return
     */
    @FormUrlEncoded
    @POST("passport/login")
    Call<String> login(@Field("phone") String phone,
                                     @Field("password") String password,
                                     @Field("type") int type,
                                     @Field("model") String model);

    /**
     * 注册验证码
     * @param phone
     * @return
     */
    @FormUrlEncoded
    @POST("passport/sendCode")
    Call<String> sendCode(@Field("phone") String phone);

    /**
     * 忘记密码验证码
     * @param phone
     * @return
     */
    @FormUrlEncoded
    @POST("passport/sendResetCode")
    Call<String> sendResetCode(@Field("phone") String phone);

    /**
     * 注册
     * @param phone
     * @param code 验证码
     * @param password
     * @return
     */
    @FormUrlEncoded
    @POST("passport/save")
    Call<String> regist(@Field("phone") String phone,
                              @Field("code") String code,
                              @Field("password") String password);

    /**
     * 获取首页内容
     * @param token
     * @return
     */
    @FormUrlEncoded
    @POST("home/getHomeInfo_v2")
    Flowable<BaseBean<HomeBean>> getHomeInfo(@Field("token") String token);

    /**
     * 获取首页消息
     * @param token
     * @return
     */
    @FormUrlEncoded
    @POST("home/getHomeMessage_v2")
    Flowable<BaseBean<HomeMsgBean>> getHomeMsg(@Field("token") String token);

    /**
     * 获取订单
     * @param token
     * @return
     */
    @FormUrlEncoded
    @POST("order/myorder")
    Call<String>
    getMyOrder(@Field("token") String token,
               @Field("page") int page,
               @Field("offset") int offset);

    /**
     * 获取订单详情
     * @param token
     * @param orderid
     * @return
     */
    @FormUrlEncoded
    @POST("order/detail")
    Call<String>
    getOrderDetail(@Field("token") String token,
               @Field("orderid") int  orderid);

    /**
     * 取消订单
     * @param token
     * @param orderid
     * @return
     */
    @FormUrlEncoded
    @POST("order/cancel")
    Call<String>
    CancelOrder(@Field("token") String token,
               @Field("orderid") int  orderid);


    /**
     * 获取取餐二维码
     * @param token
     * @param orderid
     * @return
     */
    @FormUrlEncoded
    @POST("order/getCode")
    Call<String>
    getQRCode(@Field("token") String token,
               @Field("orderid") int orderid);

    /**
     * 获取小贴士详情
     * @return
     */
//    @FormUrlEncoded
    @POST("home/articleDetail/id/{id}")
    Flowable<String>
    articleDetail(@Path("id") int id);

    /**
     * 获取小贴士列表
     * @param token
     * @param page
     * @param offset
     * @return
     */
    @FormUrlEncoded
    @POST("home/getArticles")
    Flowable<BaseBean<ArrayList<HomeBean.Article>>>
    getArticles(@Field("token") String token,
                @Field("page") int page,
                @Field("offset") int offset);

    /**
     * 返回分类列表
     * @param token
     * @param type 0=食堂 1=超市
     * @return
     */
    @FormUrlEncoded
    @POST("home/getCategories_v2")
    Flowable<BaseBean<ArrayList<OrderTypeBean>>>
    getCategories(@Field("token") String token,
                  @Field("type") int type);


    /**
     * 根据分类返回商品列表
     * @param token
     * @param catid
     * @param page
     * @param offset
     * @return
     */
    @FormUrlEncoded
    @POST("home/getProducts_v2")
    Flowable<BaseBean<ArrayList<GoodsBean>>>
    getProducts(@Field("token") String token,
                @Field("catid") int catid,
                @Field("page") int page,
                @Field("offset") int offset);

    /**
     * 添加或更新购物车
     * @param token
     * @param productid
     * @param amount
     * @return
     */
    @FormUrlEncoded
    @POST("shoppingcart/addtocart")
    Call<String>
    addtocart(@Field("token") String token,
                @Field("productid") int productid,
                @Field("amount") int amount);

    /**
     * 通过商品ID移除购物车中的商品
     * @param token
     * @param productid
     * @return
     */
    @FormUrlEncoded
    @POST("shoppingcart/remove")
    Call<String>
    removeGood(@Field("token") String token,
                @Field("productid") int productid);


    /**
     * 获取购物车信息
     * @param token
     * @return
     */
    @FormUrlEncoded
    @POST("shoppingcart/getShoppingcart")
    Call<String> getCart(@Field("token") String token);

    /**
     * 获取个人信息
     * @param token
     * @return
     */
    @FormUrlEncoded
    @POST("user/userinfo")
    Call<String>
    getUserinfo(@Field("token") String token);

    /**
     * 确认食堂订单
     * @param token
     * @return
     */
    @FormUrlEncoded
    @POST("order/confirm2")
    Call<String>
    confirmOrder(@Field("token") String token);

    /**
     * 确认超市订单
     * @param token
     * @return
     */
    @FormUrlEncoded
    @POST("order/confirm3")
    Call<String> confirmShop(@Field("token") String token);

    /**
     * 保存食堂订单并余额支付
     * @param token
     * @return
     */
    @FormUrlEncoded
    @POST("order/saveOrder2")
    Flowable<BaseBean<String>>
    saveOrder(@Field("token") String token,
              @Field("book") String book);

    /**
     * 保存超市订单并支付
     * @param token
     * @param type 支付方式 3余额支付，4线下支付
     * @return
     */
    @FormUrlEncoded
    @POST("order/saveOrder3")
    Flowable<BaseBean<String>>
    saveShop(@Field("token") String token,
              @Field("type") int type);

    /**
     * 获取消息列表
     * @param token
     * @param page
     * @param offset
     * @return
     */
    @FormUrlEncoded
    @POST("home/getMessage")
    Flowable<BaseBean<ArrayList<HomeMsgBean.Message>>>
    getMessage(@Field("token") String token,
              @Field("page") int page,
               @Field("offset") int offset);

    /**
     *清空购物车
     * @param token
     * @return
     */
    @FormUrlEncoded
    @POST("shoppingcart/clear")
    Flowable<BaseBean<String>>
    clearCart(@Field("token") String token);

    /**
     * 获取支付二维码
     * @param token
     * @return
     */
    @FormUrlEncoded
    @POST("user/getCode")
    Call<String>
    getCode(@Field("token") String token);



    @POST("home/getRecProducts")
    Call<BaseBean> getRecProducts(@Query("token") String token);


}
