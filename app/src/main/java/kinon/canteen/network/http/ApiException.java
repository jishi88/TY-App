package kinon.canteen.network.http;

/**
 * Created by lhqq on 2017-12-19.
 */

public class ApiException extends Exception {

    private int code;
    private String error;

    public ApiException(String msg) {
        super(msg);
    }

    public ApiException(int code, String msg) {
        super(msg);
        this.code = code;
        this.error = error;
    }

    public ApiException(String msg, int code, String error) {
        super(msg);
        this.code = code;
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
