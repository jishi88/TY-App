package kinon.canteen.network.http;

import rx.Subscriber;

/**
 * Created by luohao on 2017-07-04.
 */

public abstract class MySubscriber <T> extends Subscriber<T> {
    @Override
    public void onCompleted() {
        onMyCompleted();
    }

    @Override
    public void onError(Throwable e) {
        String stringErr = e.getLocalizedMessage();
        if(stringErr.contains("UnknownHostException")){
            onMyError("无法连接服务器，请检查网络是否正常");
        }
        else{
            onMyError(stringErr);
        }
    }

    @Override
    public void onNext(T t) {


    }

    public abstract void onMyNext(T t);

    public abstract void onMyError(String error);

    public abstract void onMyCompleted();

}
