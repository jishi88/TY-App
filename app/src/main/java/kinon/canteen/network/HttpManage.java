package kinon.canteen.network;



import android.text.TextUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import kinon.canteen.bean.ApiBean;
import kinon.canteen.bean.ConfirmOrderBean;
import kinon.canteen.bean.HomeMsgBean;
import kinon.canteen.bean.SendCodeBean;
import kinon.canteen.bean.ShopCartBean;
import kinon.canteen.bean.UpCartBean;
import kinon.canteen.bean.GoodsBean;
import kinon.canteen.bean.HomeBean;
import kinon.canteen.bean.OrderQRCodeBean;
import kinon.canteen.bean.OrderTypeBean;
import kinon.canteen.bean.UserBean;
import kinon.canteen.bean.base.BaseBean;
import kinon.canteen.bean.order.DetailBean;
import kinon.canteen.bean.order.OrderBean;
import kinon.canteen.network.cookie.SimpleCookieJar;
import kinon.canteen.network.http.HttpUrl;
import kinon.canteen.network.http.LogInterceptor;
import kinon.canteen.network.http.RequestApi;
import kinon.canteen.network.http.ResponseConverterFactory;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by luohao on 2017-07-04.
 */

public class HttpManage {

    private static final int TIEE_OUT=10; //超时参数

    private static Retrofit retrofit=null;
    public static RequestApi requestApi=null;
    private static RequestApi getUrlApi=null;
    private static HttpManage httpManage;



    public static HttpManage getInstance(){
        if(httpManage==null){
            httpManage=new HttpManage();
        }
        if(requestApi==null &&
                !TextUtils.isEmpty(HttpUrl.serverUrl)){
            httpManage.setRequestApi();
        }
        return httpManage;
    }

    private HttpManage(){

        Retrofit getUrlRetrofit=new Retrofit.Builder()
                .baseUrl(HttpUrl.getServerUrl)
                .addConverterFactory(ScalarsConverterFactory.create())//增加返回值为string的支持
                .addConverterFactory(GsonConverterFactory.create()) // 添加Gson转换器
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create()) // 添加Rx适配器
                .client(setHttpClient())
                .build();

        getUrlApi=getUrlRetrofit.create(RequestApi.class);
    }
    private void setRequestApi(){
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(HttpUrl.serverUrl)
//                .addConverterFactory(ResponseConverterFactory.create()) // 添加Gson转换器
                .addConverterFactory(ScalarsConverterFactory.create())//增加返回值为string的支持
                .addConverterFactory(GsonConverterFactory.create()) // 添加Gson转换器
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create()) // 添加Rx适配器
                .client(setHttpClient())
                .build();
        requestApi=retrofit.create(RequestApi.class);
    }

    private OkHttpClient setHttpClient(){
        OkHttpClient.Builder builder=new OkHttpClient.Builder()
                .connectTimeout(TIEE_OUT,TimeUnit.SECONDS)
                .readTimeout(TIEE_OUT,TimeUnit.SECONDS)
                .writeTimeout(TIEE_OUT,TimeUnit.SECONDS)
//                .retryOnConnectionFailure(true) //失败重发
                .addInterceptor(new LogInterceptor());
        return builder.build();
    }

    public Call<String> getApi(String phone){
        return getUrlApi.getApi(phone);
    }
    public Call<String> login(String phone, String password,
                                        int type, String model){
        return requestApi.login(phone,password,type,model);
    }
    public Call<String> sendCode(String phone){
        return requestApi.sendCode(phone);
    }
    public Call<String> sendResetCode(String phone){
        return requestApi.sendResetCode(phone);
    }
    public Call<String> regist(String phone,String code,
                                     String password){
        return requestApi.regist(phone,code,password);
    }
    public Flowable<BaseBean<HomeBean>> getHomeInfo(String token){
        return requestApi.getHomeInfo(token);
    }
    public Flowable<BaseBean<HomeMsgBean>> getHomeMsg(String token){
        return requestApi.getHomeMsg(token);
    }
    public Call<String> getMyOrder(String token, int page, int offset){
        return requestApi.getMyOrder(token,page,offset);
    }
    public Call<String> getOrderDetail(String token, int orderid){
        return requestApi.getOrderDetail(token,orderid);
    }
    public Call<String> CancelOrder(String token, int orderid){
        return requestApi.CancelOrder(token,orderid);
    }
    public Call<String> getQRCode(String token, int orderid){
        return requestApi.getQRCode(token,orderid);
    }
    public Flowable<String> articleDetail(int id){
        return requestApi.articleDetail(id);
    }
    public Flowable<BaseBean<ArrayList<HomeBean.Article>>> getArticles(String token,int page,int offset){
        return requestApi.getArticles(token,page,offset);
    }
    public Flowable<BaseBean<ArrayList<OrderTypeBean>>> getCategories(String token, int type){
        return requestApi.getCategories(token,type);
    }
    public Flowable<BaseBean<ArrayList<GoodsBean>>>
    getProducts(String token, int catid,int page,int offset){
        return requestApi.getProducts(token,catid,page,offset);
    }

    public Call<String> addtocart(String token, int productid,int amount){
        return requestApi.addtocart(token,productid,amount);
    }
    public Call<String> removeGood(String token, int productid){
        return requestApi.removeGood(token,productid);
    }

    public Call<String> getCart(String token){
        return requestApi.getCart(token);
    }
    public Call<String>getUserinfo(String token){
        return requestApi.getUserinfo(token);
    }
    public Call<String> confirmOrder(String token){
        return requestApi.confirmOrder(token);
    }
    public Call<String>confirmShop(String token){
        return requestApi.confirmShop(token);
    }
    public Flowable<BaseBean<String>>saveOrder(String token,String book){
        return requestApi.saveOrder(token,book);
    }
    public Flowable<BaseBean<String>>saveShop(String token,int type){
        return requestApi.saveShop(token,type);
    }
    public Flowable<BaseBean<ArrayList<HomeMsgBean.Message>>>getMessage(String token,int page,int offset){
        return requestApi.getMessage(token,page,offset);
    }

    public Call<String> getCode(String token){
        return requestApi.getCode(token);
    }
    public Flowable<BaseBean<String>>clearCart(String token){
        return requestApi.clearCart(token);
    }







   private static void getApi(){
       OkHttpClient.Builder builder = new OkHttpClient.Builder();
       builder.addInterceptor(new LogInterceptor());
       builder.connectTimeout(10, TimeUnit.SECONDS);
       builder.readTimeout(TIEE_OUT,TimeUnit.SECONDS);
       builder.writeTimeout(TIEE_OUT,TimeUnit.SECONDS);
       builder.cookieJar(new SimpleCookieJar());
       builder.followRedirects(true);

       OkHttpClient client = builder.build();
       OkHttpClient okBuilder=new OkHttpClient().newBuilder()
                .addInterceptor(new LogInterceptor())
//               .cookieJar(new SimpleCookieJar());
               //为构建者填充超时时间
               .connectTimeout(TIEE_OUT, TimeUnit.SECONDS)
               .readTimeout(TIEE_OUT,TimeUnit.SECONDS)
               .writeTimeout(TIEE_OUT,TimeUnit.SECONDS)
               //允许逆向
               .followRedirects(true)
               .build();
       retrofit=new Retrofit.Builder()
               .baseUrl(HttpUrl.serverUrl)
               .addConverterFactory(GsonConverterFactory.create())
               .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
               .client(client)
               .build();
       requestApi=retrofit.create(RequestApi.class);
   }

   public static RequestApi getRequestApi(){
       if(requestApi==null){
           getApi();
       }
       return requestApi;
   }
}
