package kinon.canteen.application;

/**
 * Created by lhqq on 2018-01-05.
 */

public class Constant {

    //跳转类
    public static final int HOME_FRAGMENT=0x01;
    public static final int SHOP_CART_FRAGMENT=0x03;
    public static final int PAY_ACTIVITY=0x04;
    public static final int PAY_CART_ACTIVITY=0x05;
    /**权限相关*/
    public static final int READ_PHONE_STATE_CODE=0x01;
}
