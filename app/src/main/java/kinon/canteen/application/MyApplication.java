package kinon.canteen.application;

import android.app.Application;

/**
 * Created by luohao on 2017-07-03.
 */

public class MyApplication extends Application {
    private static MyApplication mApplication=null;

    @Override
    public void onCreate() {
        super.onCreate();
        mApplication=this;
    }

    public static MyApplication getInstance(){
        return mApplication;
    }


    private void initJPush() {
//        JPushInterface.setDebugMode(true);
//        JPushInterface.init(this);
    }
}
