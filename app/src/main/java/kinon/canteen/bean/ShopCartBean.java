package kinon.canteen.bean;

import java.util.ArrayList;

/**
 * Created by lhqq on 2018-01-04.
 */

public class ShopCartBean {
    private int amount;
    private String total;
    private String advise;
    private ArrayList<Carts> shoppingcarts;

    public class  Carts{
        private int id;
        private int productid;
        private int amount;
        private String name;
        private String content;
        private double price;
        private String unit;
        private String shelve;
        private int stock;
        //限购
        private int perlim;
        private int daylim;
        private int type;
        private int categoryid;
        private String cname;
        private int lim;
//        private int buyamount;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getProductid() {
            return productid;
        }

        public void setProductid(int productid) {
            this.productid = productid;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getShelve() {
            return shelve;
        }

        public void setShelve(String shelve) {
            this.shelve = shelve;
        }

        public int getStock() {
            return stock;
        }

        public void setStock(int stock) {
            this.stock = stock;
        }

        public int getPerlim() {
            return perlim;
        }

        public void setPerlim(int perlim) {
            this.perlim = perlim;
        }

        public int getDaylim() {
            return daylim;
        }

        public void setDaylim(int daylim) {
            this.daylim = daylim;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getCategoryid() {
            return categoryid;
        }

        public void setCategoryid(int categoryid) {
            this.categoryid = categoryid;
        }

        public String getCname() {
            return cname;
        }

        public void setCname(String cname) {
            this.cname = cname;
        }

        public int getLim() {
            return lim;
        }

        public void setLim(int lim) {
            this.lim = lim;
        }

//        public int getBuyamount() {
//            return buyamount;
//        }
//
//        public void setBuyamount(int buyamount) {
//            this.buyamount = buyamount;
//        }
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getAdvise() {
        return advise;
    }

    public void setAdvise(String advise) {
        this.advise = advise;
    }

    public ArrayList<Carts> getShoppingcarts() {
        return shoppingcarts;
    }

    public void setShoppingcarts(ArrayList<Carts> shoppingcarts) {
        this.shoppingcarts = shoppingcarts;
    }
}
