package kinon.canteen.bean;

import java.util.ArrayList;

/**
 * Created by luohao on 2017-07-04.
 */

public class BaseListBean<T> {
    private boolean code;
    private String msg;
    private ArrayList<T> data;





    public boolean getCode() {
        return code;
    }

    public void setCode(boolean code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ArrayList<T> getData() {
        return data;
    }

    public void setData(ArrayList<T> data) {
        this.data = data;
    }
}
