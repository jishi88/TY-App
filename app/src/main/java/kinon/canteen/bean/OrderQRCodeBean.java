package kinon.canteen.bean;

/**
 * Created by lhqq on 2017-12-29.
 */

public class OrderQRCodeBean {
    private String name;
    private int dayno;
    //二维码
    private String pic;
    //一维码
    private String barcode;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDayno() {
        return dayno;
    }

    public void setDayno(int dayno) {
        this.dayno = dayno;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
}
