package kinon.canteen.bean;

import java.io.Serializable;

/**
 * Created by lhqq on 2018-01-11.
 */

public class HomeMsgBean implements Serializable {

    public String note;
    public Message message;
    public class Message implements Serializable{
        private int id;
        private int senduserid;
        private String title;
        private String content;
        private String visitid;
        private int orderid;
        private int type;
        private int isread;
        private int adminid;
        private String time;
        private String del;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getSenduserid() {
            return senduserid;
        }

        public void setSenduserid(int senduserid) {
            this.senduserid = senduserid;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getVisitid() {
            return visitid;
        }

        public void setVisitid(String visitid) {
            this.visitid = visitid;
        }

        public int getOrderid() {
            return orderid;
        }

        public void setOrderid(int orderid) {
            this.orderid = orderid;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getIsread() {
            return isread;
        }

        public void setIsread(int isread) {
            this.isread = isread;
        }

        public int getAdminid() {
            return adminid;
        }

        public void setAdminid(int adminid) {
            this.adminid = adminid;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getDel() {
            return del;
        }

        public void setDel(String del) {
            this.del = del;
        }
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
}
