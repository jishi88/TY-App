package kinon.canteen.bean.base;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by lhqq on 2018-01-12.
 */

public class ErrorBaseBean {
    private boolean code;
    private String msg;
    private JSONObject data;

    public boolean isCode() {
        return code;
    }

    public void setCode(boolean code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }
}
