package kinon.canteen.bean.base;

/**
 * Created by luohao on 2017-07-04.
 */

public class BaseBean<T> {
    private boolean code;
    private String msg;
    private T data;

    public boolean getCode() {
        return code;
    }

    public void setCode(boolean code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


}
