package kinon.canteen.bean;

/**
 * Created by lhqq on 2018-01-17.
 */

public class RxBusCartBean {
    private int amount;
    private double total;
    private int type;

    public RxBusCartBean(int amount, double total, int type) {
        this.amount = amount;
        this.total = total;
        this.type = type;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
