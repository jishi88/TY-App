package kinon.canteen.bean;

import java.util.ArrayList;

/**
 * Created by lhqq on 2018-01-05.
 * 确认食堂订单实体类
 */

public class ConfirmOrderBean {
    private double total;
    private String book1;
    private String book2;
    private String systime;
    private ArrayList <ShopCartBean.Carts> shoppingcarts;

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getBook1() {
        return book1;
    }

    public void setBook1(String book1) {
        this.book1 = book1;
    }

    public String getBook2() {
        return book2;
    }

    public void setBook2(String book2) {
        this.book2 = book2;
    }

    public String getSystime() {
        return systime;
    }

    public void setSystime(String systime) {
        this.systime = systime;
    }

    public ArrayList<ShopCartBean.Carts> getShoppingcarts() {
        return shoppingcarts;
    }

    public void setShoppingcarts(ArrayList<ShopCartBean.Carts> shoppingcarts) {
        this.shoppingcarts = shoppingcarts;
    }
}
