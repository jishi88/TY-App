package kinon.canteen.bean;

import java.io.Serializable;
import java.util.ArrayList;

import kinon.canteen.bean.base.BaseBean;

/**
 * Created by lhqq on 2017-12-26.
 */

public class HomeBean  implements Serializable{

    private ArrayList<Pictures> pictures;
    private ArrayList<Products> products;
    private Article article;
    private ArrayList<Home> home;
    private ArrayList<Home> footer;
    private  ArrayList<Home> user;

    public ArrayList<Pictures> getPictures() {
        return pictures;
    }

    public void setPictures(ArrayList<Pictures> pictures) {
        this.pictures = pictures;
    }

    public ArrayList<Products> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Products> products) {
        this.products = products;
    }

    public Article getArticle()  {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public ArrayList<Home> getHome() {
        return home;
    }

    public void setHome(ArrayList<Home> home) {
        this.home = home;
    }

    public ArrayList<Home> getFooter() {
        return footer;
    }

    public void setFooter(ArrayList<Home> footer) {
        this.footer = footer;
    }

    public ArrayList<Home> getUser() {
        return user;
    }

    public void setUser(ArrayList<Home> user) {
        this.user = user;
    }

    //功能
    public class Home{
        int id;
        String name;
        String api;
        String title;
        int type;
        String path;
        //功能图片
        String homepage_icon;
        String user_icon;
        String footer_icon;
        String footer_icon_action;
        int sort;
        String note;
        String position;
        String url;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getApi() {
            return api;
        }

        public void setApi(String api) {
            this.api = api;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getHomepage_icon() {
            return homepage_icon;
        }

        public void setHomepage_icon(String homepage_icon) {
            this.homepage_icon = homepage_icon;
        }

        public String getUser_icon() {
            return user_icon;
        }

        public void setUser_icon(String user_icon) {
            this.user_icon = user_icon;
        }

        public String getFooter_icon() {
            return footer_icon;
        }

        public void setFooter_icon(String footer_icon) {
            this.footer_icon = footer_icon;
        }

        public String getFooter_icon_action() {
            return footer_icon_action;
        }

        public void setFooter_icon_action(String footer_icon_action) {
            this.footer_icon_action = footer_icon_action;
        }

        public int getSort() {
            return sort;
        }

        public void setSort(int sort) {
            this.sort = sort;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
    //小贴士
    public class Article implements Serializable{
        int id;
        String title;
        String pic;
        String brief;
        String content;
        String add_time;
        String show_time;
        String adminid;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public String getBrief() {
            return brief;
        }

        public void setBrief(String brief) {
            this.brief = brief;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public String getShow_time() {
            return show_time;
        }

        public void setShow_time(String show_time) {
            this.show_time = show_time;
        }

        public String getAdminid() {
            return adminid;
        }

        public void setAdminid(String adminid) {
            this.adminid = adminid;
        }
    }

    //菜品推荐
    public class Products{
        int id;
        String name;
        String image;
        String content;
        double price;
        String unit;
        int sellnum;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public int getSellnum() {
            return sellnum;
        }

        public void setSellnum(int sellnum) {
            this.sellnum = sellnum;
        }
    }

    //广告
    public class Pictures{
        int id;
        String pic;
        String url;
        String note;
        String type;
        String add_time;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }
    }

}
