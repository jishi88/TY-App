package kinon.canteen.bean.order;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by luohao on 2017-07-07.
 */

public class OrderBean implements Serializable {

    private int id;
    private String orderno;
    private String dayno;
    private String order_time;
    private String  book_time;
    private String take_time;
    private int pay_type;
    private String pay_time;
    private double total;
    private int state;
    private String note;
    private String pic;
    private int notice;
    private int print;
    private String comment;
    private ArrayList<OrderDetailBean> details;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getDayno() {
        return dayno;
    }

    public void setDayno(String dayno) {
        this.dayno = dayno;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public String getBook_time() {
        return book_time;
    }

    public void setBook_time(String book_time) {
        this.book_time = book_time;
    }

    public String getTake_time() {
        return take_time;
    }

    public void setTake_time(String take_time) {
        this.take_time = take_time;
    }

    public int getPay_type() {
        return pay_type;
    }

    public void setPay_type(int pay_type) {
        this.pay_type = pay_type;
    }

    public String getPay_time() {
        return pay_time;
    }

    public void setPay_time(String pay_time) {
        this.pay_time = pay_time;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public int getNotice() {
        return notice;
    }

    public void setNotice(int notice) {
        this.notice = notice;
    }

    public int getPrint() {
        return print;
    }

    public void setPrint(int print) {
        this.print = print;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ArrayList<OrderDetailBean> getDetails() {
        return details;
    }

    public void setDetails(ArrayList<OrderDetailBean> details) {
        this.details = details;
    }
}
