package kinon.canteen.bean;

/**
 * Created by lhqq on 2018-01-03.
 */

public class GoodsBean {
    private int id;
    private String name;
    private String image;
    private String content;
    private double price;
    private String unit;
    private int stock;
    private int perlim;
    private int daylim;
    private int amount;
    private int sellnum;
    private int buyamount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getPerlim() {
        return perlim;
    }

    public void setPerlim(int perlim) {
        this.perlim = perlim;
    }

    public int getDaylim() {
        return daylim;
    }

    public void setDaylim(int daylim) {
        this.daylim = daylim;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getSellnum() {
        return sellnum;
    }

    public void setSellnum(int sellnum) {
        this.sellnum = sellnum;
    }

    public int getBuyamount() {
        return buyamount;
    }

    public void setBuyamount(int buyamount) {
        this.buyamount = buyamount;
    }
}
