package kinon.canteen.bean;

/**
 * Created by lhqq on 2018-01-03.
 */

public class UpCartBean {
    private int amount;
    private double total;

    public UpCartBean(int amount, double total) {
        this.amount = amount;
        this.total = total;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
}
