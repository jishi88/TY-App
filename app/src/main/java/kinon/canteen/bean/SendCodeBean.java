package kinon.canteen.bean;

import kinon.canteen.bean.base.Base;

/**
 * Created by lhqq on 2018-01-12.
 */

public class SendCodeBean extends Base{

    private Phone data;

    public class Phone{
        private String phone;
        private String code;
    }

    public Phone getData() {
        return data;
    }

    public void setData(Phone data) {
        this.data = data;
    }
}
