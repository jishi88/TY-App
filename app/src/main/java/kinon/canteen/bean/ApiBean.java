package kinon.canteen.bean;

/**
 * Created by lhqq on 2017-12-25.
 */

public class ApiBean {
    private String api;

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }
}
