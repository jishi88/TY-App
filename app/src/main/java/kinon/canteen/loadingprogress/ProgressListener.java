package kinon.canteen.loadingprogress;

/**
 * Created by Administrator on 2016/10/11.
 */
public interface ProgressListener {
    void onProgress(long progress, long total, boolean done);
}
