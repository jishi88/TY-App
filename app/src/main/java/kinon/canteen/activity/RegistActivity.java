package kinon.canteen.activity;


import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import butterknife.BindView;
import io.reactivex.disposables.Disposable;
import kinon.canteen.R;
import kinon.canteen.bean.ApiBean;
import kinon.canteen.bean.SendCodeBean;
import kinon.canteen.bean.base.BaseBean;
import kinon.canteen.network.HttpManage;
import kinon.canteen.network.http.CommonSubscriber;
import kinon.canteen.network.http.HttpUrl;
import kinon.canteen.network.http.StrCallback;
import kinon.canteen.util.MyShowDialog;
import kinon.canteen.util.RxUtil;
import kinon.canteen.util.ShardPManager;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by lhqq on 2017-12-25.
 */

public class RegistActivity extends BaseActivity implements View.OnClickListener,
        View.OnFocusChangeListener{

    private RelativeLayout rl_topbar;
    private TextView tv_title;
    private ImageView img_back;
    private EditText et_phone;
    private RadioButton rb_getCode;
    private EditText et_code;
    private EditText et_password;
    private Button btn_regist;
    private ImageView img_phoneLine,img_codeLine,img_pwdLine;

    private String mPhone="";
    private boolean isRbClick=true;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_layout);
        initView();
    }

    private void initView() {
        rl_topbar= (RelativeLayout) findViewById(R.id.rl_topbar);
        tv_title= (TextView) findViewById(R.id.tv_title);
        img_back= (ImageView) findViewById(R.id.img_back);
        et_phone= (EditText) findViewById(R.id.et_phone);
        rb_getCode= (RadioButton) findViewById(R.id.rb_getCode);
        et_code= (EditText) findViewById(R.id.et_code);
        et_password= (EditText) findViewById(R.id.et_password);
        btn_regist= (Button) findViewById(R.id.btn_regist);
        img_phoneLine= (ImageView) findViewById(R.id.img_phoneLine);
        img_codeLine= (ImageView) findViewById(R.id.img_codeLine);
        img_pwdLine= (ImageView) findViewById(R.id.img_pwdLine);

        img_back.setVisibility(View.VISIBLE);
        rl_topbar.setBackgroundColor(getResources().getColor(R.color.white));
        tv_title.setText("注册");

        addListener();
    }

    private void addListener(){
        img_back.setOnClickListener(this);
        rb_getCode.setOnClickListener(this);
        btn_regist.setOnClickListener(this);
        et_phone.setOnFocusChangeListener(this);
        et_code.setOnFocusChangeListener(this);
        et_password.setOnFocusChangeListener(this);

    }




    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.img_back:
                finish();
            break;
            case R.id.rb_getCode:
                if(isRbClick){
                    rb_getCode.setChecked(false);
                    isRbClick=false;
                    phoneJudge();
                }

            break;
            case R.id.btn_regist:
                codeJudge();
            break;

        }

    }
    //手机号码判断
    private void phoneJudge(){
        mPhone=et_phone.getText().toString();
        if(TextUtils.isEmpty(mPhone)){
            rb_getCode.setChecked(true);
            isRbClick=true;
            mToast("请输入手机号");
            return;
        }
        if(mPhone.length()!=11){
            rb_getCode.setChecked(true);
            isRbClick=true;
            mToast("请输入正确的手机号");
            return;
        }
        HttpUrl.serverUrl=ShardPManager.getInstance().getStr("serverUrl");
        if(TextUtils.isEmpty(HttpUrl.serverUrl)){
            getApi(mPhone);
        }else {
            sendCode(mPhone);
        }

    }

    private void codeJudge(){
        String code=et_code.getText().toString();
        String password=et_password.getText().toString();
        if(TextUtils.isEmpty(code)){
            mToast("请输入验证码");
            return;
        }
        if(code.length()<6){
            mToast("请输入正确的验证码");
            return;
        }
        if(TextUtils.isEmpty(password)){
            mToast("请输入密码");
            return;
        }
        if(password.length()<6){
            mToast("密码长度大于6位");
            return;
        }
        regist(code,password);
    }

    private void getApi(final String phone){
        Call<String> call=HttpManage.getInstance().getApi(phone);
        call.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String data) {
                Gson gson=new Gson();
                ApiBean apiBean=gson.fromJson(data,ApiBean.class);
                rb_getCode.setChecked(true);
                isRbClick=true;
                apiDealwith(phone,apiBean.getApi());
            }

            @Override
            public void onError(int code, String error) {
                rb_getCode.setChecked(true);
                isRbClick=true;
                MyShowDialog.closeLoadingDialog();
                HttpUrl.serverUrl= ShardPManager.getInstance().getStr("serverUrl");

            }
        });
    }

    private void apiDealwith(String phone,String api){

        if(!TextUtils.isEmpty(api)){
            HttpUrl.serverUrl=api;
            ShardPManager.getInstance().putStrCommit("serverUrl",api);
        }else{
            HttpUrl.serverUrl=ShardPManager.getInstance().getStr("serverUrl");
        }
        sendCode(phone);
    }

    private void sendCode(String phone){
        Call<String> call=HttpManage.getInstance().sendCode(phone);
        call.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String data) {
                mToast("发送验证码成功");
                rb_getCode.setChecked(true);
                isRbClick=true;
            }

            @Override
            public void onError(int code, String error) {
                rb_getCode.setChecked(true);
                isRbClick=true;
                mToast(error);
            }
        });
    }

    private void regist(String code,String password){
        Call<String> call=HttpManage.getInstance()
                .regist(mPhone,code,password);
        call.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String data) {
                finish();
            }

            @Override
            public void onError(int code, String error) {
                mToast(error);
            }
        });
    }

    @Override
    public void onFocusChange(View view, boolean b) {

        switch (view.getId()){
            case R.id.et_phone:
                if(b){
                    setImgLine(img_phoneLine,img_codeLine,img_pwdLine);
                }
            break;
            case R.id.et_code:
                if(b)
                setImgLine(img_codeLine,img_phoneLine,img_pwdLine);
            break;
            case R.id.et_password:
                if(b)
                    setImgLine(img_pwdLine,img_codeLine,img_phoneLine);
            break;
        }
    }

    private void setImgLine(ImageView img1,ImageView img2,ImageView img3){
        Log.e("lh", " setImgLine=== " );
        img1.setImageResource(R.drawable.img_login_redline);
        img2.setImageResource(R.drawable.img_login_wline);
        img3.setImageResource(R.drawable.img_login_wline);
    }
}
