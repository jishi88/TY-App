package kinon.canteen.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.google.gson.Gson;

import io.reactivex.disposables.Disposable;
import kinon.canteen.R;
import kinon.canteen.adapter.OrderDetailAdapter;
import kinon.canteen.bean.base.BaseBean;
import kinon.canteen.bean.order.DetailBean;

import kinon.canteen.network.HttpManage;
import kinon.canteen.network.http.CommonSubscriber;
import kinon.canteen.network.http.StrCallback;
import kinon.canteen.util.RxUtil;
import kinon.canteen.util.ShardPManager;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by lhqq on 2017-12-27.
 */

public class OrderDetailActivity extends BaseActivity implements View.OnClickListener{

    private ImageView img_back;
    private TextView tv_title;
    private TextView tv_orderId;
    private ListView list_detail;
    private TextView tv_payPrice;
    private TextView tv_takemealId;
    private TextView tv_takeTime;
    private RelativeLayout rl_takeTime;
    private Button btn_getQRCode;
    private int mState;
    private int orderid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderdetail_layout);
        initView();
    }
    private void initView() {
        img_back= (ImageView) findViewById(R.id.img_back);
        tv_title= (TextView) findViewById(R.id.tv_title);
        tv_orderId= (TextView) findViewById(R.id.tv_orderId);
        list_detail= (ListView) findViewById(R.id.list_detail);
        tv_payPrice= (TextView) findViewById(R.id.tv_payPrice);
        tv_takemealId= (TextView) findViewById(R.id.tv_takemealId);
        tv_takeTime= (TextView) findViewById(R.id.tv_takeTime);
        rl_takeTime= (RelativeLayout) findViewById(R.id.rl_takeTime);
        btn_getQRCode= (Button) findViewById(R.id.btn_getQRCode);

        img_back.setOnClickListener(this);
        btn_getQRCode.setOnClickListener(this);
        img_back.setVisibility(View.VISIBLE);
        tv_title.setText("订单详情");
        orderid= getIntent().getIntExtra("orderid",0);
        getOrderDetail(orderid);
//        if(getIntent().getSerializableExtra("order")!=null){
//            OrderBean orderBean= (OrderBean)
//                    getIntent().getSerializableExtra("order");
//            mState=orderBean.getState();
//            orderid=orderBean.getId();
//            showView(orderBean);
//        }

    }
    private void getOrderDetail(int orderid){
        Log.e("lh", "orderid== "+orderid );
        Call<String> call=HttpManage.getInstance()
                .getOrderDetail(ShardPManager.getInstance().getToken(),orderid);
        call.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String data) {
                Gson gson=new Gson();
                DetailBean detail=gson.fromJson(data,DetailBean.class);
                showView(detail);
            }

            @Override
            public void onError(int code, String error) {
              mToast(error);
            }
        });
    }

    private void showView(DetailBean orderBean){
        mState=orderBean.getState();
        list_detail.setAdapter(new OrderDetailAdapter
                (orderBean.getDetails(),OrderDetailActivity.this));
        tv_orderId.setText("订单编号:"+orderBean.getOrderno());
        tv_payPrice.setText(String.valueOf(orderBean.getTotal()));
        tv_takemealId.setText(String.valueOf(orderBean.getDayno()));

        rl_takeTime.setVisibility(View.GONE);
        if(mState==0){ //去支付
            btn_getQRCode.setText("去支付");
        }else if(mState==1){ //取餐二维码
            btn_getQRCode.setText("领取取餐二维码");
        }else if(mState==-1){  //已取消
            tv_takemealId.setText("已取消");
            btn_getQRCode.setVisibility(View.GONE);
        }else{
            btn_getQRCode.setVisibility(View.GONE);
            rl_takeTime.setVisibility(View.VISIBLE);
            tv_takeTime.setText(orderBean.getTake_time());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_back:
            finish();
            break;
            case R.id.btn_getQRCode:
             if(mState==0){
                 Intent intent=new Intent(OrderDetailActivity.this,
                         PaymentActivity.class);
                 startActivity(intent);
                 finish();

             }else{
                 Intent intent=new Intent(OrderDetailActivity.this,
                         QRCodeActivity.class);
                 intent.putExtra("orderid",orderid);
                 startActivity(intent);
                 finish();
             }
            break;
        }
    }
}
