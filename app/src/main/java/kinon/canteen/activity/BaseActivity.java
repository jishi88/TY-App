package kinon.canteen.activity;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import butterknife.ButterKnife;
import kinon.canteen.R;
import kinon.canteen.application.Constant;

/**
 * Created by luohao on 2017-06-30.
 */

public class BaseActivity extends AppCompatActivity {

//    protected  RequestApi requestApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//       getBaseRequestApi();
    }


    /**
     * 申请指定权限
     * @param code
     * @param permission
     */
    public void requestPermission(int code,String... permission){
        ActivityCompat.requestPermissions(this,permission,code);
    }

    /**
     *判断是否有指定权限
     */
    public boolean hasPermission(String permission){
        if(ContextCompat.checkSelfPermission(this,permission)!=
                PackageManager.PERMISSION_GRANTED){
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,  String[] permissions,
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case Constant.READ_PHONE_STATE_CODE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    doSDCardPermission();
                    onReadPermission();
                    //了权限同意
                }else{
                    //拒绝权限
                    //判断版本是否是需求版本
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        // 判断用户是否 点击了不再提醒。(检测该权限是否还可以申请)
                        boolean b = shouldShowRequestPermissionRationale(permissions[0]);
                        if(!b){
                            //还想开启权限
                            onReadNoPermission();
                        }else{
                            finish();
                        }
                    }
                }
                break;
        }
    }

    /**
     * 同意了权限申请
     */
    protected void onReadPermission(){

    }

    /**
     * 申请权限是点击了否，进行处理的方法*/
    protected void onReadNoPermission(){

    }


//    private  void getBaseRequestApi() {
//        requestApi= HttpManage.getRetrofit().create(RequestApi.class);
//    }

    protected void mToast(String content){
        Toast.makeText(getApplicationContext(), content, Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
