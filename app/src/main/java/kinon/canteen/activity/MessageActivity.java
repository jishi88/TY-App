package kinon.canteen.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;

import io.reactivex.disposables.Disposable;
import kinon.canteen.R;
import kinon.canteen.adapter.MessageAdapter;
import kinon.canteen.bean.HomeMsgBean;
import kinon.canteen.bean.base.BaseBean;
import kinon.canteen.network.HttpManage;
import kinon.canteen.network.http.CommonSubscriber;
import kinon.canteen.util.RxUtil;
import kinon.canteen.util.ShardPManager;

/**
 * Created by lhqq on 2018-01-11.
 */

public class MessageActivity extends BaseActivity implements View.OnClickListener,
        AdapterView.OnItemClickListener{
    private ImageView img_back;
    private TextView tv_title;
    private SmartRefreshLayout smart_msg;
    private ListView list_msg;

    private int page=30;
    private int offset=0;
    private ArrayList<HomeMsgBean.Message> messageList;
    private MessageAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_layout);
        initView();
    }

    private void initView() {
        img_back= (ImageView) findViewById(R.id.img_back);
        tv_title= (TextView) findViewById(R.id.tv_title);
        smart_msg= (SmartRefreshLayout) findViewById(R.id.smart_msg);
        list_msg= (ListView) findViewById(R.id.list_msg);

        img_back.setVisibility(View.VISIBLE);
        tv_title.setText("信息列表");
        getMessage();
        addListener();
        messageList=new ArrayList<>();
        adapter=new MessageAdapter(messageList,this);
        list_msg.setAdapter(adapter);
    }

    private void getMessage(){
        Disposable dis= HttpManage.getInstance().getMessage(
                ShardPManager.getInstance().getToken(),page,offset)
                .compose(RxUtil.<BaseBean<ArrayList<HomeMsgBean.Message>>>rxSchedulerHelper())
                .compose(RxUtil.<ArrayList<HomeMsgBean.Message>>handleResult())
                .subscribeWith(new CommonSubscriber<ArrayList<HomeMsgBean.Message>>() {
                    @Override
                    public void onNext(ArrayList<HomeMsgBean.Message> messages) {
                        messageList.addAll(messages);
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFail(Throwable t, int code, String msg) {
                    }
                });
    }

    private void addListener() {
        setSmartRefreshLayout();
        img_back.setOnClickListener(this);
        list_msg.setOnItemClickListener(this);

    }

    @Override
    public void onClick(View view) {
        finish();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if(messageList.get(i).getType()==1){
            Intent intent=new Intent(MessageActivity.this,OrderDetailActivity.class);
            intent.putExtra("orderid",messageList.get(i).getOrderid());
            startActivity(intent);
        }else{
            Intent intent=new Intent(MessageActivity.this,MsgDetailActivity.class);
            intent.putExtra("message",messageList.get(i));
            startActivity(intent);
        }

    }

    private void setSmartRefreshLayout(){
        smart_msg.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                refreshlayout.finishRefresh(2*1000);
                messageList.clear();
                offset=0;
                getMessage();
            }
        });
        smart_msg.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                refreshlayout.finishLoadmore(2*1000);
                offset += page;
                getMessage();
            }
        });
    }
}
