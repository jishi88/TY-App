package kinon.canteen.activity;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import io.reactivex.disposables.Disposable;
import kinon.canteen.R;
import kinon.canteen.bean.HomeBean;
import kinon.canteen.network.HttpManage;
import kinon.canteen.network.http.CommonSubscriber;
import kinon.canteen.util.RxUtil;
import kinon.canteen.util.glide.GlideUtils;

/**
 * Created by lhqq on 2017-12-29.
 */

public class TipsContextActivity extends BaseActivity implements View.OnClickListener {

    private TextView tv_tip;
    private ImageView img_back;
    private TextView tv_title;
    private TextView tv_tipTitel;
    private TextView tv_tipTime;
    private ImageView img_pic;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipcontext_layout);
        initView();
    }

    private void initView() {
        img_back= (ImageView) findViewById(R.id.img_back);
        tv_tip= (TextView) findViewById(R.id.tv_tip);
        tv_title= (TextView) findViewById(R.id.tv_title);
        tv_tipTitel= (TextView) findViewById(R.id.tv_tipTitel);
        tv_tipTime= (TextView) findViewById(R.id.tv_tipTime);
        img_pic= (ImageView) findViewById(R.id.img_pic);

        img_back.setVisibility(View.VISIBLE);
        img_back.setOnClickListener(this);
        tv_title.setText("营养小贴士");

        if(getIntent().getSerializableExtra("tipcontent")!=null){
            HomeBean.Article tip= (HomeBean.Article)
                    getIntent().getSerializableExtra("tipcontent") ;
            tv_tipTitel.setText(tip.getTitle());
            tv_tipTime.setText(tip.getShow_time());
            tv_tip.setText(tip.getContent());
            tv_tip.setText(Html.fromHtml(tip.getContent()));
            GlideUtils.getInstance().displayImage
                    (this,tip.getPic(),img_pic);
//            getTipDetail(tip.getId());
        }

//        tv_tip.setText(Html.fromHtml(tipContent));
    }

    private void getTipDetail(int id){
        Disposable dis= HttpManage.getInstance().articleDetail(id)
                .compose(RxUtil.<String>rxSchedulerHelper())
                .subscribeWith(new CommonSubscriber<String>() {
                    @Override
                    public void onNext(String s) {
                        tv_tip.setText(Html.fromHtml(s));
                    }

                    @Override
                    public void onFail(Throwable t, int code, String msg) {

                    }
                });

    }

    @Override
    public void onClick(View view) {
        finish();
    }
}
