package kinon.canteen.activity;

import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import kinon.canteen.R;
import kinon.canteen.util.LoadDialogManager;
import kinon.canteen.util.MyShowDialog;
import kinon.canteen.util.NetWorkUtil;
import kinon.canteen.util.ShardPManager;

/**
 * Created by lhqq on 2017-12-27.
 */

public class WebActivity extends BaseActivity implements View.OnClickListener{

    private ImageView img_back;
    private TextView tv_title;
    private WebView web_view;
    private TextView tv_off;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_layout);
       initView();
    }

    private void initView() {
        img_back= (ImageView) findViewById(R.id.img_back);
        tv_title= (TextView) findViewById(R.id.tv_title);
        tv_off= (TextView) findViewById(R.id.tv_off);
        web_view= (WebView) findViewById(R.id.web_view);
        img_back.setVisibility(View.VISIBLE);
        img_back.setOnClickListener(this);
        tv_off.setOnClickListener(this);
//        String titled=
        String url=getIntent().getStringExtra("url");
        String title=getIntent().getStringExtra("title");
        tv_title.setText(TextUtils.isEmpty(title)?"":title);
        showWeb(url);
    }

    private void showWeb(String url){
        if(TextUtils.isEmpty(url)){
            mToast("加载信息失败");
            finish();
            return;
        }
        Log.e("lh", "url =="+url );
        url=url+"?token="+ ShardPManager.getInstance().getToken()
                +"&header=0&type=native";
        web_view.loadUrl(url);

        web_view.setWebViewClient(new WebViewClient(){

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                web_view.loadUrl(request.getUrl().toString());
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                MyShowDialog.showLoadingDialog(WebActivity.this);

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                MyShowDialog.closeLoadingDialog();
            }
        });

        WebSettings settings = web_view.getSettings();
        // 缓存(cache)
        settings.setAppCacheEnabled(true);
//        settings.setAppCachePath(context.getCacheDir().getAbsolutePath());
        // 存储(storage)
        settings.setDomStorageEnabled(true);    // 默认值 false
        settings.setDatabaseEnabled(true);      // 默认值 false
        //自适应手机屏幕
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        // 是否支持Javascript，默认值false
        settings.setJavaScriptEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // 5.0以上允许加载http和https混合的页面(5.0以下默认允许，5.0+默认禁止)
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        if (NetWorkUtil.isNetwork(WebActivity.this)) {
            // 根据cache-control决定是否从网络上取数据
            settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        } else {
            // 没网，离线加载，优先加载缓存(即使已经过期)
            settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_back:
                if(web_view.canGoBack()){
                    web_view.goBack();
                }else{
                    finish();
                }
                break;
            case R.id.tv_off:
                finish();
                break;
        }

    }

    @Override
    public void onBackPressed() {
        if(web_view.canGoBack()){
            web_view.goBack();
        }else{
            super.onBackPressed();
//            finish();
        }
    }
}
