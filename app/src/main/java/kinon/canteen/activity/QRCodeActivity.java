package kinon.canteen.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import io.reactivex.disposables.Disposable;
import kinon.canteen.R;
import kinon.canteen.bean.OrderQRCodeBean;
import kinon.canteen.bean.base.BaseBean;
import kinon.canteen.network.HttpManage;
import kinon.canteen.network.http.CommonSubscriber;
import kinon.canteen.network.http.StrCallback;
import kinon.canteen.util.RxUtil;
import kinon.canteen.util.ShardPManager;
import kinon.canteen.util.glide.GlideUtils;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by lhqq on 2017-12-27.
 */

public class QRCodeActivity extends BaseActivity implements View.OnClickListener{

    private ImageView img_back;
    private TextView tv_title;
    private TextView tv_name;
    private TextView tv_takeId;
    private ImageView img_oneCode;
    private ImageView img_qrCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode_layout);
        initView();
    }
    private void initView() {
        img_back= (ImageView) findViewById(R.id.img_back);
        tv_title= (TextView) findViewById(R.id.tv_title);
        tv_name= (TextView) findViewById(R.id.tv_name);
        tv_takeId= (TextView) findViewById(R.id.tv_takeId);
        img_oneCode= (ImageView) findViewById(R.id.img_oneCode);
        img_qrCode= (ImageView) findViewById(R.id.img_qrCode);
        int orderid=getIntent().getIntExtra("orderid",-1);
        tv_title.setText("取餐二维码");
        img_back.setVisibility(View.VISIBLE);
        img_back.setOnClickListener(this);
        getqrCode(orderid);
    }

    private void getqrCode(int orderid){
        Log.e("lh", "orderid== "+orderid );
        Call<String> call=HttpManage.getInstance()
                .getQRCode(ShardPManager.getInstance().getToken(),orderid);
        call.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String data) {
                Gson gson=new Gson();
                OrderQRCodeBean orderQR=gson.fromJson(data,OrderQRCodeBean.class);
                shwoView(orderQR);
            }

            @Override
            public void onError(int code, String error) {
              mToast(error);
            }
        });

    }

    private void shwoView(OrderQRCodeBean codeBean){
        tv_name.setText(codeBean.getName());
        tv_takeId.setText(codeBean.getDayno()+"");
        GlideUtils.getInstance().displayImage(QRCodeActivity.this,
                codeBean.getPic(),img_qrCode);
        GlideUtils.getInstance().displayImage(QRCodeActivity.this,
                codeBean.getBarcode(),img_oneCode);
    }

    @Override
    public void onClick(View view) {
        finish();
    }
}
