package kinon.canteen.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioButton;

import java.util.ArrayList;

import kinon.canteen.R;
import kinon.canteen.application.Constant;
import kinon.canteen.fragment.home.HomeFragment;
import kinon.canteen.fragment.home.OrderFragment;
import kinon.canteen.fragment.home.PersonalFragment;
import kinon.canteen.fragment.home.QRCodeFragment;
import kinon.canteen.fragment.home.ShoppingCartFragment;

/**
 * ************************************************************************
 * **                              _oo0oo_                               **
 * **                             o8888888o                              **
 * **                             88" . "88                              **
 * **                             (| -_- |)                              **
 * **                             0\  =  /0                              **
 * **                           ___/'---'\___                            **
 * **                        .' \\\|     |// '.                          **
 * **                       / \\\|||  :  |||// \\                        **
 * **                      / _ ||||| -:- |||||- \\                       **
 * **                      | |  \\\\  -  /// |   |                       **
 * **                      | \_|  ''\---/''  |_/ |                       **
 * **                      \  .-\__  '-'  __/-.  /                       **
 * **                    ___'. .'  /--.--\  '. .'___                     **
 * **                 ."" '<  '.___\_<|>_/___.' >'  "".                  **
 * **                | | : '-  \'.;'\ _ /';.'/ - ' : | |                 **
 * **                \  \ '_.   \_ __\ /__ _/   .-' /  /                 **
 * **            ====='-.____'.___ \_____/___.-'____.-'=====             **
 * **                              '=---='                               **
 * ************************************************************************
 * **                        佛祖保佑      镇类之宝                         **
 * ************************************************************************
 *
 */

public class MainActivity extends BaseActivity implements OnClickListener{

    private RadioButton rb_home;
    private RadioButton rb_cart;
    private RadioButton rb_QRCode;
    private RadioButton rb_order;
    private RadioButton rb_personal;

    private ArrayList<Fragment> fragments=new ArrayList<>();
    private FragmentManager fm=null;
    //fragment的下标
    private int fragmentIndex=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        rb_home= (RadioButton) findViewById(R.id.rb_home);
        rb_cart= (RadioButton) findViewById(R.id.rb_cart);
        rb_QRCode= (RadioButton) findViewById(R.id.rb_QRCode);
        rb_order= (RadioButton) findViewById(R.id.rb_order);
        rb_personal= (RadioButton) findViewById(R.id.rb_personal);

        addFragments();
        addOnclick();
    }

    private void addFragments(){
        fragments.add(new HomeFragment());
        fragments.add(new ShoppingCartFragment());
        fragments.add(new QRCodeFragment());
        fragments.add(new OrderFragment());
        fragments.add(new PersonalFragment());

        showFristFragment();
//        showFragment(-1);

    }

    private void addOnclick(){
        rb_home.setOnClickListener(this);
        rb_cart.setOnClickListener(this);
        rb_QRCode.setOnClickListener(this);
        rb_order.setOnClickListener(this);
        rb_personal.setOnClickListener(this);

    }


    private void showFristFragment(){
        fm=getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.fl_home, fragments.get(0));
        fragmentTransaction.commit();

    }

    private void showFragment(int position){

        if(fragmentIndex!=position){
            FragmentTransaction transction=fm.beginTransaction();
            //隐藏之前显示的fragment
            transction.hide(fragments.get(fragmentIndex));
            //判断fragment是否加载过，
            if(!fragments.get(position).isAdded()){
                transction.add(R.id.fl_home,fragments.get(position));
            }
            transction.show(fragments.get(position));
            transction.commit();
            fragmentIndex=position;
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.rb_home:
                showFragment(0);
                break;
            case R.id.rb_cart:
                showFragment(1);
                break;
            case R.id.rb_QRCode:
                showFragment(2);
                break;
            case R.id.rb_order:
                showFragment(3);
                break;
            case R.id.rb_personal:
                showFragment(4);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("lh", "requestCode== "+requestCode );
        Log.e("lh", "resultCode== "+resultCode );
        if(resultCode==4){
            rb_cart.setChecked(true);
            showFragment(1);
        }else if(resultCode==Constant.PAY_CART_ACTIVITY){
            rb_cart.setChecked(true);
            showFragment(1);
        }

    }
}
