package kinon.canteen.activity;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import kinon.canteen.R;
import kinon.canteen.bean.HomeMsgBean;

/**
 * Created by lhqq on 2018-01-17.
 */

public class MsgDetailActivity extends BaseActivity implements View.OnClickListener{

    private ImageView img_back;
    private TextView tv_title;
    private TextView tv_msgTime;
    private TextView tv_msgContent;
    private TextView tv_msgTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_msgdetail_layout);
        initView();
    }

    private void initView() {
        img_back= (ImageView) findViewById(R.id.img_back);
        tv_title= (TextView) findViewById(R.id.tv_title);
        tv_msgTitle= (TextView) findViewById(R.id.tv_msgTitle);
        tv_msgTime= (TextView) findViewById(R.id.tv_msgTime);
        tv_msgContent= (TextView) findViewById(R.id.tv_msgContent);
        img_back.setVisibility(View.VISIBLE);
        tv_title.setText("消息详情");
        addListener();
        HomeMsgBean.Message msg = null;
        if(getIntent().getSerializableExtra("message")!=null){
            msg= (HomeMsgBean.Message) getIntent().getSerializableExtra("message");
        }
//        HomeMsgBean msg=getIntent().getSerializableExtra("message")==null?null:
//                (HomeMsgBean)getIntent().getSerializableExtra("message");
        if(msg!=null){
            tv_msgTime.setText(msg.getTime());
            tv_msgTitle.setText(msg.getTitle());
            tv_msgContent.setText(Html.fromHtml(msg.getContent()));
        }

    }

    private void addListener() {
        img_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_back:
                finish();
                break;
        }
    }
}
