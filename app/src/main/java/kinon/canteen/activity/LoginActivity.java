package kinon.canteen.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import io.reactivex.disposables.Disposable;
import kinon.canteen.R;
import kinon.canteen.bean.ApiBean;
import kinon.canteen.bean.base.BaseBean;
import kinon.canteen.network.HttpManage;
import kinon.canteen.network.http.CommonSubscriber;
import kinon.canteen.network.http.HttpUrl;
import kinon.canteen.network.http.StrCallback;
import kinon.canteen.util.MyShowDialog;
import kinon.canteen.util.RxUtil;
import kinon.canteen.util.ShardPManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by luohao on 2017-07-03.
 */

public class LoginActivity extends BaseActivity implements OnClickListener{
    private EditText et_phone;
    private EditText et_password;
    private ImageView img_line_phone;
    private ImageView img_line_pwd;
    private Button btn_login;
    private TextView tv_forgetPwd;
    private TextView tv_register;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_layout);
        initView();
    }

    private void initView() {
//        674a31747ccec201a2dc40c3f81dec0d
        et_phone= (EditText) findViewById(R.id.et_phone);
        et_password= (EditText) findViewById(R.id.et_password);
        img_line_phone= (ImageView) findViewById(R.id.img_line_phone);
        img_line_pwd= (ImageView) findViewById(R.id.img_line_pwd);
        btn_login= (Button) findViewById(R.id.btn_login);
        tv_forgetPwd= (TextView) findViewById(R.id.tv_forgetPwd);
        tv_register= (TextView) findViewById(R.id.tv_register);

        et_phone.setText(ShardPManager.getInstance().getStr("phone"));
        et_password.setText(ShardPManager.getInstance().getStr("password"));
        btn_login.setOnClickListener(this);
        tv_forgetPwd.setOnClickListener(this);
        tv_register.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_login:
                userLogin();
                break;
            case R.id.tv_forgetPwd:
                Intent ForgetIntent=new Intent(LoginActivity.this,
                        ForgetPwdActivity.class);
                startActivity(ForgetIntent);
                break;
            case R.id.tv_register:
                Intent intent=new Intent(LoginActivity.this,
                        RegistActivity.class);
                startActivity(intent);
                break;
        }
    }


    private void userLogin() {
        String phone=et_phone.getText().toString().trim();
        if(TextUtils.isEmpty(phone)){
//            Toast.makeText(getApplicationContext(),"")
            mToast("请输入您的手机号码");
            return;
        }
        String password=et_password.getText().toString().trim();
        if(TextUtils.isEmpty(password)){
            mToast("请输入您的密码");
        }

        getApi(phone,password);
    }

    private void getApi(final String phone,final String password){

        MyShowDialog.showLoadingDialog(LoginActivity.this);
        Call<String> call=HttpManage.getInstance().getApi(phone);

        call.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String data) {
                Gson gson=new Gson();
                ApiBean apiBean=gson.fromJson(data,ApiBean.class);
                apiDealwith(phone,password,apiBean.getApi());
            }

            @Override
            public void onError(int code, String error) {
                MyShowDialog.closeLoadingDialog();
                HttpUrl.serverUrl=ShardPManager.getInstance().getStr("serverUrl");

            }
        });

    }
    private void apiDealwith(String phone,String password,String api){

        if(!TextUtils.isEmpty(api)){
            HttpUrl.serverUrl=api;
            ShardPManager.getInstance().putStrCommit("serverUrl",api);
        }else{
            HttpUrl.serverUrl=ShardPManager.getInstance().getStr("serverUrl");
        }

        login(phone,password,2);
    }

    private void login(final String phone ,final String password,int type){
        String model= Build.MODEL;
        Call<String> loginCall=HttpManage.getInstance()
                .login(phone,password,type,model);
        loginCall.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String data) {
                MyShowDialog.closeLoadingDialog();
                ShardPManager.getInstance().putStrCommit("phone",phone);
                ShardPManager.getInstance().putStrCommit("password",password);
                ShardPManager.getInstance().putStrCommit("token",data);
                Intent intent=new Intent(LoginActivity.this,
                        MainActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onError(int code, String error) {
                MyShowDialog.closeLoadingDialog();
                mToast(error);
            }
        });
    }
}
