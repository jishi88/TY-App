package kinon.canteen.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.io.Serializable;
import java.util.ArrayList;

import io.reactivex.disposables.Disposable;
import kinon.canteen.R;
import kinon.canteen.adapter.TipsAdapter;
import kinon.canteen.bean.HomeBean;
import kinon.canteen.bean.base.BaseBean;
import kinon.canteen.network.HttpManage;
import kinon.canteen.network.http.CommonSubscriber;
import kinon.canteen.util.RxUtil;
import kinon.canteen.util.ShardPManager;

/**
 * Created by lhqq on 2017-12-31.
 */

public class TipsActivity extends BaseActivity implements View.OnClickListener,
        AdapterView.OnItemClickListener{

    private ImageView img_back;
    private TextView tv_title;
    private SmartRefreshLayout smart_tip;
    private ListView list_tip;
    private int page=20;
    private int offset=0;

    private ArrayList<HomeBean.Article> articleList;
    private TipsAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips_layout);
        initView();
    }

    private void initView() {
        img_back= (ImageView) findViewById(R.id.img_back);
        tv_title= (TextView) findViewById(R.id.tv_title);
        smart_tip= (SmartRefreshLayout) findViewById(R.id.smart_tip);
        list_tip= (ListView) findViewById(R.id.list_tip);

        tv_title.setText("饮食小贴士");
        img_back.setVisibility(View.VISIBLE);
        img_back.setOnClickListener(this);
        list_tip.setOnItemClickListener(this);
        articleList=new ArrayList<>();
        adapter=new TipsAdapter(articleList,this);
        list_tip.setAdapter(adapter);
        getTips();
        setSmartRefreshLayout();
    }

    private void getTips(){
        Disposable dis= HttpManage.getInstance()
                .getArticles(ShardPManager.getInstance().getToken(),page,offset)
                .compose(RxUtil.<BaseBean<ArrayList<HomeBean.Article>>>rxSchedulerHelper())
                .compose(RxUtil.<ArrayList<HomeBean.Article>>handleResult())
                .subscribeWith(new CommonSubscriber<ArrayList<HomeBean.Article>>() {
                    @Override
                    public void onNext(ArrayList<HomeBean.Article> artice) {

                        showView(artice);
                    }

                    @Override
                    public void onFail(Throwable t, int code, String msg) {

                    }
                });
    }

    private void showView(ArrayList<HomeBean.Article> artice){
        articleList.addAll(artice);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        finish();
    }

    private void setSmartRefreshLayout(){
        smart_tip.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                refreshlayout.finishRefresh(2*1000);
                articleList.clear();
                offset=0;
                getTips();
            }
        });
        smart_tip.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                refreshlayout.finishLoadmore(2*1000);
                offset += page;
                getTips();
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intetn=new Intent(TipsActivity.this,
                TipsContextActivity.class);
        intetn.putExtra("tipcontent",
                (Serializable) articleList.get(i));
        startActivity(intetn);
    }
}
