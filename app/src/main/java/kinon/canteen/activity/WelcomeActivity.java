package kinon.canteen.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;
import kinon.canteen.R;
import kinon.canteen.application.Constant;
import kinon.canteen.bean.ApiBean;
import kinon.canteen.bean.base.BaseBean;
import kinon.canteen.network.HttpManage;
import kinon.canteen.network.http.CommonSubscriber;
import kinon.canteen.network.http.HttpUrl;
import kinon.canteen.util.RxUtil;

/**
 * Created by lhqq on 2017-11-29.
 */

public class WelcomeActivity extends BaseActivity {

    @BindView(R.id.img_welcome)
     ImageView mImgWelcome;
    @BindView(R.id.tv_author)
    TextView mTvAuthor;

    private boolean isHome=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_layout);
        ButterKnife.bind(this);
//        getApi();
//        permission();
        mImgWelcome.postDelayed(countRunnable,1*1000);
    }

    private void permission(){
        if(!hasPermission(Manifest.permission.READ_SMS) ||
                hasPermission(Manifest.permission.READ_PHONE_STATE ) ){
            requestPermission(Constant.READ_PHONE_STATE_CODE,
                    new String[]{Manifest.permission.READ_SMS,
                            Manifest.permission.READ_PHONE_STATE});

        }
    }

    @Override
    protected void onReadPermission() {
        super.onReadPermission();
//        getApi();
    }

    @Override
    protected void onReadNoPermission() {
        super.onReadNoPermission();
        permission();
    }





    private Runnable countRunnable=new Runnable() {
        @Override
        public void run() {
            goHome();
        }
    };

    private synchronized void goHome(){
        if(!isHome){
            isHome=true;
            Intent intent=new Intent(WelcomeActivity.this,LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mImgWelcome.removeCallbacks(countRunnable);
        countRunnable=null;
        isHome=true;
    }
}
