package kinon.canteen.activity;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.Gson;

import java.util.Calendar;

import io.reactivex.disposables.Disposable;
import kinon.canteen.R;
import kinon.canteen.adapter.ConfirmOrderAdapter;
import kinon.canteen.application.Constant;
import kinon.canteen.bean.ConfirmOrderBean;
import kinon.canteen.bean.base.BaseBean;
import kinon.canteen.network.HttpManage;
import kinon.canteen.network.http.CommonSubscriber;
import kinon.canteen.network.http.StrCallback;
import kinon.canteen.util.RxUtil;
import kinon.canteen.util.ShardPManager;
import kinon.canteen.view.MyListView;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by lhqq on 2018-01-04.
 */

public class PaymentActivity extends BaseActivity implements View.OnClickListener {

    private ImageView img_back;
    private TextView tv_title;
    private TextView tv_pay;
    private TextView tv_payPrice;
    private MyListView list_pay;
    private RelativeLayout rl_takeTime;
    private RelativeLayout rl_payType;
    private TextView tv_takeTime;

    private int mGoodType;

    private ConfirmOrderAdapter adapter;

    private double startTime;
    private double endTime;
    private boolean isSetTakeTime=true;
    private String sysDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_layout);

        initView();
    }

    private void initView() {
        img_back= (ImageView) findViewById(R.id.img_back);
        tv_title= (TextView) findViewById(R.id.tv_title);
        tv_pay= (TextView) findViewById(R.id.tv_pay);
        list_pay= (MyListView) findViewById(R.id.list_pay);
        tv_payPrice= (TextView) findViewById(R.id.tv_payPrice);
        rl_takeTime= (RelativeLayout) findViewById(R.id.rl_takeTime);
        rl_payType= (RelativeLayout) findViewById(R.id.rl_payType);
        tv_takeTime= (TextView) findViewById(R.id.tv_takeTime);

        img_back.setVisibility(View.VISIBLE);
        img_back.setOnClickListener(this);
        tv_takeTime.setOnClickListener(this);
        tv_pay.setOnClickListener(this);
        tv_title.setText("结算");
        mGoodType=getIntent().getIntExtra("mGoodType",0);
        if(mGoodType==0){
            rl_takeTime.setVisibility(View.VISIBLE);
            confirmOrder();
        }else{
            rl_payType.setVisibility(View.VISIBLE);
            confirmShop();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_back:
                finish();
                break;
            case R.id.tv_pay:
                if(mGoodType==0){
                    saveOrder();
                }else{
                    saveShop();
                }

                break;
            case R.id.tv_takeTime:
                if(!isSetTakeTime){
                    mToast("暂时不能设置取餐时间");
                    return;
                }
                showTimeDialog();
                break;
        }
    }
    //确认食堂订单
    private void confirmOrder(){
        Call<String> call= HttpManage.getInstance()
                .confirmOrder(ShardPManager.getInstance().getToken());
        call.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String data) {
                Gson gson=new Gson();
                ConfirmOrderBean bean=gson.fromJson(data,ConfirmOrderBean.class);
                showOrder(bean);
            }

            @Override
            public void onError(int code, String error) {
              mToast(error);
                finish();
            }
        });
    }

    private void showOrder(ConfirmOrderBean bean){
        tv_payPrice.setText(String.valueOf(bean.getTotal()));
        sysDate=bean.getSystime().substring(0,10);
        tv_takeTime.setText(sysDate+"\t"+bean.getBook1());
        adapter=new ConfirmOrderAdapter(bean.getShoppingcarts(),
                PaymentActivity.this);
        list_pay.setAdapter(adapter);
        startTime= Integer.parseInt(bean.getBook1().substring(0,2))
                +Double.parseDouble(bean.getBook1().substring(3,5))/60;
        endTime=Integer.parseInt(bean.getBook2().substring(0,2))
                +Double.parseDouble(bean.getBook2().substring(3,5))/60;
        if(startTime==endTime){
            isSetTakeTime=false;
        }
    }

    //确认超市订单
    private void confirmShop(){
        Call<String> call= HttpManage.getInstance()
                .confirmShop(ShardPManager.getInstance().getToken());
        call.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String data) {
                Gson gson=new Gson();
                ConfirmOrderBean bean=gson.fromJson(data,ConfirmOrderBean.class);
                showOrder(bean);
            }

            @Override
            public void onError(int code, String error) {
                mToast(error);
                finish();
            }
        });
    }

    private void showTimeDialog(){
        Calendar calendar= Calendar.getInstance(); //通过Calendar获取系统时间
        TimePickerDialog timeDialog=new
                TimePickerDialog(PaymentActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int h, int m) {
               double selectTime=h+(double)m/60;
                if(startTime<selectTime && selectTime<endTime){
                  tv_takeTime.setText(sysDate+"\t"+h+":"+m);
                }else{
                    mToast("不在订餐时间内");
                }

            }
        },calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),true);
        timeDialog.show();
    }

    private void saveOrder(){
        Disposable disp=HttpManage.getInstance()
                .saveOrder(ShardPManager.getInstance().getToken(),
                        tv_takeTime.getText().toString())
                .compose(RxUtil.<BaseBean<String>>rxSchedulerHelper())
                .compose(RxUtil.<String>handleResult())
                .subscribeWith(new CommonSubscriber<String>() {
                    @Override
                    public void onNext(String s) {
                        mToast("支付成功");
                       Intent intent=new Intent(PaymentActivity.this,
                               MainActivity.class);
                        setResult(Constant.PAY_ACTIVITY,intent);
//                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onFail(Throwable t, int code, String msg) {

                    }
                });

    }

    private void saveShop(){
        Disposable disp=HttpManage.getInstance()
                .saveShop(ShardPManager.getInstance().getToken(),
                        4)
                .compose(RxUtil.<BaseBean<String>>rxSchedulerHelper())
                .compose(RxUtil.<String>handleResult())
                .subscribeWith(new CommonSubscriber<String>() {
                    @Override
                    public void onNext(String s) {
                        mToast("支付成功");
                       Intent intent=new Intent(PaymentActivity.this,
                               MainActivity.class);
                        setResult(Constant.PAY_ACTIVITY,intent);
//                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onFail(Throwable t, int code, String msg) {

                    }
                });

    }



//    private void showShop(ConfirmOrderBean bean){
//        tv_payPrice.setText(String.valueOf(bean.getTotal()));
//
//    }

}
