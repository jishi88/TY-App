package kinon.canteen.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kinon.canteen.R;
import kinon.canteen.activity.OrderFoodActivity;
import kinon.canteen.activity.WebActivity;
import kinon.canteen.application.Constant;
import kinon.canteen.bean.HomeBean;
import kinon.canteen.util.glide.GlideUtils;

/**
 * Created by lhqq on 2018-01-04.
 */

public class PersonalIcomAdapter extends RecyclerView.Adapter<PersonalIcomAdapter.ViewHolder>
       /* implements View.OnClickListener*/ {

    private LayoutInflater inflater;
    private ArrayList<HomeBean.Home> list;
    private Context context;
//    private MyItemClickListener mListener;
//
//    public  interface MyItemClickListener {
//        void onItemClick(View view , int position);
//    }
//    public void setMyItemListener(MyItemClickListener listener){
//        mListener=listener;
//    }

    public PersonalIcomAdapter(ArrayList<HomeBean.Home> list, Context context) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.item_homeicom_layout,parent,false);
//        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GlideUtils.displayImage(context,list.get(position)
                .getUser_icon(),holder.img_homeicom);
        holder.tv_icomName.setText(list.get(position).getTitle());
//        holder.img_homeicom.setOnClickListener(this);
        holder.ll_homeicom.setOnClickListener(new MyItemClickListener(holder.ll_homeicom,position));
    }

    



    @Override
    public int getItemCount() {
        return list.size();
    }

    /*@Override
    public void onClick(View view) {
        int posision=(int)view.getTag();
        Intent intent=new Intent(context, WebActivity.class);
        intent.putExtra("url",list.get(posision).getUrl());
        intent.putExtra("title",list.get(posision).getTitle());
        context.startActivity(intent);
//          mListener.onItemClick(view,posision );
    }*/

//    public

    public  class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView img_homeicom;
        private TextView tv_icomName;
        private LinearLayout ll_homeicom;
        public ViewHolder(View itemView) {
            super(itemView);
            img_homeicom= (ImageView) itemView.findViewById(R.id.img_homeicom);
            tv_icomName= (TextView) itemView.findViewById(R.id.tv_icomName);
            ll_homeicom= (LinearLayout) itemView.findViewById(R.id.ll_homeicom);
        }
    }

    private class MyItemClickListener implements View.OnClickListener{
         LinearLayout view;
        int position;

        public MyItemClickListener(LinearLayout view,int position){
           this.view=view;
            this.position=position;
        }

        @Override
        public void onClick(View view) {
            if(list.get(position).getName().equals("shop") /*||
                            mHomeBean.getHome().get(i).getId()==11*/){
                intentFoodActivity(1);

            }else if(list.get(position).getName().equals("buy_food")
                            /*||mHomeBean.getHome().get(i).getId()==3*/){
                intentFoodActivity(0);
            }else{
                Intent intent=new Intent(context, WebActivity.class);
                intent.putExtra("url",list.get(position).getUrl());
                intent.putExtra("title",list.get(position).getTitle());
                context.startActivity(intent);
            }
        }
        private void intentFoodActivity(int type){
            Intent food=new Intent(context,
                    OrderFoodActivity.class);
            food.putExtra("goodType",type);
            context.startActivity(food);
        }
    }


}
