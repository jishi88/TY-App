package kinon.canteen.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import kinon.canteen.R;
import kinon.canteen.bean.HomeBean;
import kinon.canteen.util.glide.GlideUtils;

/**
 * Created by lhqq on 2017-12-26.
 */

public class HomeIcomAdapter extends BaseAdapter {

    private ArrayList<HomeBean.Home> list;
    private Context context;

    public HomeIcomAdapter(ArrayList<HomeBean.Home> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Viewholder holder;
        if(view==null){
            holder=new Viewholder();
            view= LayoutInflater.from(context).inflate
                    (R.layout.item_homeicom_layout,null);
            holder.img_homeicom= (ImageView) view.findViewById(R.id.img_homeicom);
            holder.tv_icomName= (TextView) view.findViewById(R.id.tv_icomName);
            view.setTag(holder);
        }else{
            holder= (Viewholder) view.getTag();
        }
        GlideUtils.getInstance().displayImage(context,
                list.get(i).getHomepage_icon(),holder.img_homeicom);
        holder.tv_icomName.setText(list.get(i).getTitle());
        return view;
    }

    class  Viewholder{
        ImageView img_homeicom;
        TextView tv_icomName;
    }
}
