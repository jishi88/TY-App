package kinon.canteen.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import kinon.canteen.R;
import kinon.canteen.bean.HomeBean;
import kinon.canteen.util.glide.GlideUtils;

/**
 * Created by lhqq on 2017-12-26.
 */

public class TipsAdapter extends BaseAdapter {

    private ArrayList<HomeBean.Article> list;
    private Context context;

    public TipsAdapter(ArrayList<HomeBean.Article> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Viewholder holder;
        if(view==null){
            holder=new Viewholder();
            view= LayoutInflater.from(context).inflate
                    (R.layout.item_tips_layout,null);
            holder.img_tip= (ImageView) view.findViewById(R.id.img_tip);
            holder.tv_tipTitle= (TextView) view.findViewById(R.id.tv_tipTitle);
            holder.tv_tipTime= (TextView) view.findViewById(R.id.tv_tipTime);
            view.setTag(holder);
        }else{
            holder= (Viewholder) view.getTag();
        }
        GlideUtils.getInstance().displayImage(context,
                list.get(i).getPic(),holder.img_tip);
        holder.tv_tipTitle.setText(list.get(i).getTitle());
        holder.tv_tipTime.setText(list.get(i).getShow_time());
        return view;
    }

    class  Viewholder{
        ImageView img_tip;
        TextView tv_tipTitle;
        TextView tv_tipTime;
    }
}
