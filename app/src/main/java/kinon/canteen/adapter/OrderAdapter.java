package kinon.canteen.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;

import io.reactivex.disposables.Disposable;
import kinon.canteen.R;
import kinon.canteen.activity.OrderDetailActivity;
import kinon.canteen.activity.QRCodeActivity;
import kinon.canteen.bean.HomeBean;
import kinon.canteen.bean.base.BaseBean;
import kinon.canteen.bean.order.OrderBean;
import kinon.canteen.network.HttpManage;
import kinon.canteen.network.http.CommonSubscriber;
import kinon.canteen.network.http.StrCallback;
import kinon.canteen.util.RxUtil;
import kinon.canteen.util.ShardPManager;
import kinon.canteen.util.glide.GlideUtils;
import kinon.canteen.view.dialog.CustomDialog;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by lhqq on 2017-12-26.
 */

public class OrderAdapter extends BaseAdapter {

    private ArrayList<OrderBean> list;
    private Context context;

    public OrderAdapter(ArrayList<OrderBean> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i,  View view, ViewGroup viewGroup) {
        Viewholder holder;
        if(view==null){
            holder=new Viewholder();
            view= LayoutInflater.from(context).inflate
                    (R.layout.item_order_layout,null);
            holder.tv_goodsName= (TextView) view.findViewById(R.id.tv_goodsName);
            holder.tv_orderSatus= (TextView) view.findViewById(R.id.tv_orderSatus);
            holder.tv_orderTime= (TextView) view.findViewById(R.id.tv_orderTime);
            holder.tv_orderPrice= (TextView) view.findViewById(R.id.tv_orderPrice);
            holder.tv_orderPay= (TextView) view.findViewById(R.id.tv_orderPay);
            holder.tv_orderCancel= (TextView) view.findViewById(R.id.tv_orderCancel);
            view.setTag(holder);
        }else{
            holder= (Viewholder) view.getTag();
        }
        if(list.get(i).getDetails().size()>0){
            holder.tv_goodsName.setText(list.get(i)
                    .getDetails().get(0).getName()+"等"
                    +list.get(i).getDetails().size()+"个菜品");
        }else{
            holder.tv_goodsName.setText("无菜品");
        }


        holder.tv_orderTime.setText(list.get(i).getOrder_time());
        holder.tv_orderPrice.setText(String.valueOf(list.get(i).getTotal()));

        switch (list.get(i).getState()){
            case -1:
                holder.tv_orderSatus.setText("已取消");
                holder.tv_orderPay.setText("订单详情");
                holder.tv_orderPay.setBackgroundResource(R.drawable.btn_order_detail);
                holder.tv_orderCancel.setVisibility(View.GONE);
            break;
            case 0:
                holder.tv_orderSatus.setText("待支付");
                holder.tv_orderPay.setText("马上支付");
                holder.tv_orderPay.setBackgroundResource(R.drawable.btn_order_pay);
                holder.tv_orderCancel.setVisibility(View.VISIBLE);
                break;
            case 1:
                holder.tv_orderSatus.setText("待取货");
                holder.tv_orderPay.setText("取餐二维码");
                holder.tv_orderPay.setBackgroundResource(R.drawable.btn_order_detail);
                holder.tv_orderCancel.setVisibility(View.VISIBLE);
                break;
            case 2:
                holder.tv_orderSatus.setText("已完成");
                holder.tv_orderPay.setText("订单详情");
                holder.tv_orderPay.setBackgroundResource(R.drawable.btn_order_detail);
                holder.tv_orderCancel.setVisibility(View.GONE);
                break;
        }
        holder.tv_orderPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                orderPayDealwith(i);
            }
        });
        holder.tv_orderCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomDialog dialog=new CustomDialog(context, "是否确认取消订单", new CustomDialog.DialogClickListener() {
                    @Override
                    public void onDialogClick(int btn) {
                        if(btn==1){
                          cancelOrder(i,list.get(i).getId());
                        }
                    }
                });
                dialog.show();

            }
        });
        return view;
    }

    //订单详情处理
    private void orderPayDealwith(int i){
        int state=list.get(i).getState();
        if(state==0){ //去支付
            Intent intent=new Intent(context,
                    OrderDetailActivity.class);

            context.startActivity(intent);
        }else if(state==1){ //取餐二维码
            Intent codeIntent=new Intent(context,
                    QRCodeActivity.class);
            codeIntent.putExtra("orderid",list.get(i).getId());
            context.startActivity(codeIntent);
        }else{  //详情
            Intent intent=new Intent(context,
                    OrderDetailActivity.class);
            intent.putExtra("order", (Serializable) list.get(i));
//            intent.putExtra("order_state",state);
            context.startActivity(intent);
        }
    }

    private void cancelOrder(final int position, int orderid){
        Call<String> call=HttpManage.getInstance()
                .CancelOrder(ShardPManager.getInstance().getToken(),orderid);
        call.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String data) {
                list.get(position).setState(-1);
                notifyDataSetChanged();
            }

            @Override
            public void onError(int code, String error) {
                Toast.makeText(context,error,Toast.LENGTH_SHORT).show();
            }
        });
    }

    class  Viewholder{
        TextView tv_goodsName;
        TextView tv_orderSatus;
        TextView tv_orderTime;
        TextView tv_orderPrice;
        TextView tv_orderPay;
        TextView tv_orderCancel;
    }
}
