package kinon.canteen.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import kinon.canteen.R;
import kinon.canteen.bean.order.OrderDetailBean;

/**
 * Created by lhqq on 2017-12-29.
 */

public class OrderDetailAdapter extends BaseAdapter {

    private ArrayList<OrderDetailBean> list;
    private Context context;

    public OrderDetailAdapter(ArrayList<OrderDetailBean> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view==null){
            holder=new ViewHolder();
            view= LayoutInflater.from(context)
                    .inflate(R.layout.item_order_detail,null);
            holder.tv_goodsName= (TextView) view.findViewById(R.id.tv_goodsName);
            holder.tv_goodsNum= (TextView) view.findViewById(R.id.tv_goodsNum);
            holder.tv_goodsPrice= (TextView) view.findViewById(R.id.tv_goodsPrice);
            view.setTag(holder);
        }else{
            holder= (ViewHolder) view.getTag();
        }
        holder.tv_goodsName.setText(list.get(i).getName());
        holder.tv_goodsNum.setText("×"+String.valueOf(list.get(i).getAmount()));
        holder.tv_goodsPrice.setText("￥"+String.valueOf(list.get(i).getPrice()));
        return view;
    }

    private  class ViewHolder{
        TextView tv_goodsName;
        TextView tv_goodsNum;
        TextView tv_goodsPrice;
    }
}
