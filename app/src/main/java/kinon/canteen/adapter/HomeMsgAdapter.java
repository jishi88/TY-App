package kinon.canteen.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

import kinon.canteen.R;
import kinon.canteen.bean.HomeBean;
import kinon.canteen.util.glide.GlideUtils;

/**
 * Created by lhqq on 2017-12-26.
 */

public class HomeMsgAdapter extends BaseAdapter {

    private ArrayList<HomeBean.Home> list;
    private Context context;

    public HomeMsgAdapter(ArrayList<HomeBean.Home> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Viewholder holder;
        if(view==null){
            holder=new Viewholder();
            view= LayoutInflater.from(context).inflate
                    (R.layout.item_home_msg,null);
            holder.img_homemsg= (ImageView) view.findViewById(R.id.img_homemsg);
            holder.tv_msgdate= (TextView) view.findViewById(R.id.tv_msgdate);
            holder.tv_msgtitle= (TextView) view.findViewById(R.id.tv_msgtitle);
            holder.rb_msg= (RadioButton) view.findViewById(R.id.rb_msg);
            view.setTag(holder);
        }else{
            holder= (Viewholder) view.getTag();
        }
        GlideUtils.getInstance().displayImage(context,
                list.get(i).getFooter_icon(),holder.img_homemsg);
        holder.tv_msgdate.setText(list.get(i).getTitle());
        holder.tv_msgtitle.setText(list.get(i).getTitle());
        if(list.get(i).getType()==1){
            holder.rb_msg.setChecked(true);
        }else{
            holder.rb_msg.setChecked(false);
        }
        return view;
    }

    class  Viewholder{
        ImageView img_homemsg;
        TextView tv_msgdate;
        TextView tv_msgtitle;
        RadioButton rb_msg;
    }
}
