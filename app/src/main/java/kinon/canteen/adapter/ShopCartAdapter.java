package kinon.canteen.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import kinon.canteen.R;
import kinon.canteen.bean.RxBusCartBean;
import kinon.canteen.bean.ShopCartBean;
import kinon.canteen.bean.UpCartBean;
import kinon.canteen.network.HttpManage;
import kinon.canteen.network.http.StrCallback;
import kinon.canteen.util.RxBus;
import kinon.canteen.util.ShardPManager;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by lhqq on 2017-12-26.
 */

public class ShopCartAdapter extends BaseAdapter {

    private ArrayList<ShopCartBean.Carts> list;
    private Context context;
    private int mGoodType;
    private onAdapterClickListener adapterListener;

    public interface onAdapterClickListener{
        void onAdapterListener(int position, UpCartBean bean);
    }

    public void setAdapterListener(onAdapterClickListener listener){
        adapterListener=listener;
    }


    public ShopCartAdapter(ArrayList<ShopCartBean.Carts> list, Context context, int goodType) {
        this.list = list;
        this.context = context;
        mGoodType=goodType;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final Viewholder holder;
        if(view==null){
            holder=new Viewholder();
            view= LayoutInflater.from(context).inflate
                    (R.layout.item_cart_layout,null);

            holder.tv_cart_good= (TextView) view.findViewById(R.id.tv_cart_good);
            holder.tv_goodIntegral= (TextView) view.findViewById(R.id.tv_goodIntegral);
            holder.tv_goodNum= (TextView) view.findViewById(R.id.tv_goodNum);
            holder.img_cart_less= (ImageView) view.findViewById(R.id.img_cart_less);
            holder.img_cart_add= (ImageView) view.findViewById(R.id.img_cart_add);
            view.setTag(holder);
        }else{
            holder= (Viewholder) view.getTag();
        }

        holder.tv_cart_good.setText(list.get(i).getName());
        holder.tv_goodNum.setText(String.valueOf(list.get(i).getAmount()));
        holder.tv_goodIntegral.setText("￥"+String.valueOf(list.get(i).getPrice()));
        if(mGoodType==0){ //食堂
        }else{//超市
        }

        holder.img_cart_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if(adapterListener!=null){
                int num=list.get(i).getAmount();

                if(mGoodType==0 && list.get(i).getPerlim()>0){
                    if(num+1>list.get(i).getPerlim()){
                        Toast.makeText(context,"限购"+list.get(i).getPerlim()
                                +list.get(i).getUnit(),Toast.LENGTH_SHORT).show();
                        return;
                    }
                    updateCart(holder.tv_goodNum,list.get(i).getProductid(),num+1,i,0);
                }else{
                    updateCart(holder.tv_goodNum,list.get(i).getProductid(),num+1,i,0);
                }
                }
//            }
        });
        holder.img_cart_less.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if(adapterListener!=null){

                    int num=list.get(i).getAmount();
                    if(num-1<0){
                        Toast.makeText(context,"数量不能小于0",Toast.LENGTH_SHORT).show();
                    }else if (num-1==0){
                      removeGood(holder.tv_goodNum,list.get(i).getProductid(),num-1,i);
                    }else{
                     updateCart(holder.tv_goodNum,list.get(i).getProductid(),num-1,i,1);
                    }


                }
//            }
        });


        return view;
    }

    private void updateCart(final TextView tvNum, int productid,
                            final int amount, final int position,final int addLess){
        Call<String> call= HttpManage.getInstance().addtocart
                (ShardPManager.getInstance().getToken(),productid, amount);
        call.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String data) {
                list.get(position).setAmount(amount);
                tvNum.setText(String.valueOf(amount));
//                RxBus.getDefault().post(new UpCartBean(1,list.get(position).getPrice()));
                if(addLess==0){
                    RxBus.getDefault().post(new RxBusCartBean(1,
                            list.get(position).getPrice(),mGoodType));
                }else{
                    RxBus.getDefault().post(new RxBusCartBean(-1,
                            -(list.get(position).getPrice()),mGoodType));
                }
            }

            @Override
            public void onError(int code, String error) {
                Toast.makeText(context,error,Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void removeGood(final TextView tvNum, int productid, final int amount, final int position){
        Call<String> call=HttpManage.getInstance().removeGood
                (ShardPManager.getInstance().getToken(),productid);
        call.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String data) {
                list.get(position).setAmount(amount);
                tvNum.setText(String.valueOf(amount));
                RxBus.getDefault().post(new RxBusCartBean(-1,
                        -list.get(position).getPrice(),mGoodType));
                list.remove(position);
                notifyDataSetChanged();

            }

            @Override
            public void onError(int code, String error) {
                Toast.makeText(context,error,Toast.LENGTH_SHORT).show();
            }
        });
    }

    class  Viewholder{

        ImageView img_cart_less;
        ImageView img_cart_add;
        TextView tv_cart_good;
//        TextView tv_foodLave;
        TextView tv_goodIntegral;
        TextView tv_goodNum;

    }
}
