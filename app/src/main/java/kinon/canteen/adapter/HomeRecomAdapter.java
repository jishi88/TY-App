package kinon.canteen.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

import kinon.canteen.R;
import kinon.canteen.bean.HomeBean;
import kinon.canteen.util.glide.GlideUtils;

/**
 * Created by lhqq on 2017-12-26.
 */

public class HomeRecomAdapter extends BaseAdapter {

    private ArrayList<HomeBean.Products> list;
    private Context context;

    public HomeRecomAdapter(ArrayList<HomeBean.Products> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Viewholder holder;
        if(view==null){
            holder=new Viewholder();
            view= LayoutInflater.from(context).inflate
                    (R.layout.item_home_recom,null);
            holder.img_recom= (ImageView) view.findViewById(R.id.img_recom);
            holder.tv_disheName= (TextView) view.findViewById(R.id.tv_disheName);
            holder.tv_dishePice= (TextView) view.findViewById(R.id.tv_dishePice);
            holder.tv_disheuUnit= (TextView) view.findViewById(R.id.tv_disheuUnit);
            view.setTag(holder);
        }else{
            holder= (Viewholder) view.getTag();
        }
        GlideUtils.getInstance().displayImage(context,
                list.get(i).getImage(),holder.img_recom);
        holder.tv_disheName.setText(list.get(i).getName());
        holder.tv_dishePice.setText(list.get(i).getPrice()+"");
        holder.tv_disheuUnit.setText(list.get(i).getUnit());
        return view;
    }

    class  Viewholder{
        ImageView img_recom;
        TextView tv_disheName;
        TextView tv_dishePice;
        TextView tv_disheuUnit;
    }
}
