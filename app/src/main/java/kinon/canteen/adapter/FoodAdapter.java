package kinon.canteen.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import kinon.canteen.R;
import kinon.canteen.bean.UpCartBean;
import kinon.canteen.bean.GoodsBean;
import kinon.canteen.bean.base.BaseBean;
import kinon.canteen.network.HttpManage;
import kinon.canteen.network.http.CommonSubscriber;
import kinon.canteen.network.http.MyFunction;
import kinon.canteen.network.http.StrCallback;
import kinon.canteen.util.RxBus;
import kinon.canteen.util.RxUtil;
import kinon.canteen.util.ShardPManager;
import kinon.canteen.util.glide.GlideUtils;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by lhqq on 2017-12-26.
 */

public class FoodAdapter extends BaseAdapter {

    private ArrayList<GoodsBean> list;
    private Context context;
    private int mGoodType;
    private onAdapterClickListener adapterListener;

    public interface onAdapterClickListener{
        void onAdapterListener(int position,UpCartBean bean);
    }

    public void setAdapterListener(onAdapterClickListener listener){
        adapterListener=listener;
    }


    public FoodAdapter(ArrayList<GoodsBean> list, Context context,int goodType) {
        this.list = list;
        this.context = context;
        mGoodType=goodType;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final Viewholder holder;
        if(view==null){
            holder=new Viewholder();
            view= LayoutInflater.from(context).inflate
                    (R.layout.item_food_layout,null);
            holder.img_food= (ImageView) view.findViewById(R.id.img_food);
            holder.tv_foodName= (TextView) view.findViewById(R.id.tv_foodName);
            holder.tv_foodLave= (TextView) view.findViewById(R.id.tv_foodLave);
            holder.tv_integral= (TextView) view.findViewById(R.id.tv_integral);
            holder.tv_foodNum= (TextView) view.findViewById(R.id.tv_foodNum);
            holder.img_less= (ImageView) view.findViewById(R.id.img_less);
            holder.img_add= (ImageView) view.findViewById(R.id.img_add);
            view.setTag(holder);
        }else{
            holder= (Viewholder) view.getTag();
        }
        GlideUtils.getInstance().displayImage(context,
                list.get(i).getImage(),holder.img_food);
        holder.tv_foodName.setText(list.get(i).getName());
        holder.tv_integral.setText(String.valueOf(list.get(i).getPrice()));
        holder.tv_foodNum.setText(String.valueOf(list.get(i).getAmount()));
        if(mGoodType==0){ //食堂
            if(list.get(i).getPerlim()==0){
                holder.tv_foodLave.setText("月销量"+list.get(i)
                        .getSellnum()+",剩余"+list.get(i).getStock());
            }else{
                holder.tv_foodLave.setText("限购"+list.get(i)
                        .getPerlim()+",剩余"+list.get(i).getStock());
            }
        }else{//超市
            holder.tv_foodLave.setText("剩余"+list.get(i).getStock());
        }

        holder.img_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if(adapterListener!=null){
                int num=list.get(i).getAmount();
                if(mGoodType==0 && list.get(i).getPerlim()>0){
                    if(num+1>list.get(i).getPerlim()){
                        Toast.makeText(context,"限购"+list.get(i).getPerlim()
                                +list.get(i).getUnit(),Toast.LENGTH_SHORT).show();
                        return;
                    }
                    updateCart(holder.tv_foodNum,list.get(i).getId(),num+1,i,0);
                }else{
                    updateCart(holder.tv_foodNum,list.get(i).getId(),num+1,i,0);
                }
                }
//            }
        });
        holder.img_less.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    int num=list.get(i).getAmount();
                    if(num-1<0){
                        Toast.makeText(context,"数量不能小于0",Toast.LENGTH_SHORT).show();
                    }else if (num-1==0){
                      removeGood(holder.tv_foodNum,list.get(i).getId(),num-1,i);
                    }else{
                     updateCart(holder.tv_foodNum,list.get(i).getId(),num-1,i,1);
                    }


                }
//            }
        });


        return view;
    }

    private void updateCart(final TextView tvNum, int productid, final int amount,
                            final int position, final int addLess){
        Call<String> call= HttpManage.getInstance().addtocart
                (ShardPManager.getInstance().getToken(),productid, amount);
        call.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String data) {
                list.get(position).setAmount(amount);
                tvNum.setText(String.valueOf(amount));
                if(addLess==0){
                    RxBus.getDefault().post(new UpCartBean(1,list.get(position).getPrice()));
                }else{
                    RxBus.getDefault().post(new UpCartBean(-1,-(list.get(position).getPrice())));
                }

            }

            @Override
            public void onError(int code, String error) {
              Toast.makeText(context,error,Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void removeGood(final TextView tvNum, int productid, final int amount, final int position){
        Call<String> call=HttpManage.getInstance().removeGood
                (ShardPManager.getInstance().getToken(),productid);
        call.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String data) {
                list.get(position).setAmount(amount);
                tvNum.setText(String.valueOf(amount));
                RxBus.getDefault().post(new UpCartBean(-1,-list.get(position).getPrice()));
            }

            @Override
            public void onError(int code, String error) {
                Toast.makeText(context,error,Toast.LENGTH_SHORT).show();
            }
        });
    }

    class  Viewholder{
        ImageView img_food;
        ImageView img_less;
        ImageView img_add;
        TextView tv_foodName;
        TextView tv_foodLave;
        TextView tv_integral;
        TextView tv_foodNum;

    }
}
