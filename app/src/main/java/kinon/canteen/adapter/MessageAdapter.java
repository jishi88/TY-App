package kinon.canteen.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import kinon.canteen.R;
import kinon.canteen.bean.HomeMsgBean;


/**
 * Created by lhqq on 2017-12-26.
 */

public class MessageAdapter extends BaseAdapter {

    private ArrayList<HomeMsgBean.Message> list;
    private Context context;

    public MessageAdapter(ArrayList<HomeMsgBean.Message> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Viewholder holder;
        if(view==null){
            holder=new Viewholder();
            view= LayoutInflater.from(context).inflate
                    (R.layout.iten_msg_layout,null);
            holder.img_msg= (ImageView) view.findViewById(R.id.img_msg);
            holder.tv_msgData= (TextView) view.findViewById(R.id.tv_msgData);
            holder.tv_msgContent= (TextView) view.findViewById(R.id.tv_msgContent);
            view.setTag(holder);
        }else{
            holder= (Viewholder) view.getTag();
        }
//        GlideUtils.getInstance().displayImage(context,
//                list.get(i).getPic(),holder.img_tip);
        holder.img_msg.setImageResource(R.drawable.img_msg_dinner);
        holder.tv_msgData.setText(list.get(i).getTime());
        holder.tv_msgContent.setText(Html.fromHtml(list.get(i).getContent()));
        return view;
    }

    class  Viewholder{
        ImageView img_msg;
        TextView tv_msgData;
        TextView tv_msgContent;
    }
}
