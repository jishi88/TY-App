package kinon.canteen.fragment.home;

import android.content.Intent;
import android.os.Bundle;


import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import kinon.canteen.R;
import kinon.canteen.activity.MessageActivity;
import kinon.canteen.adapter.PersonalIcomAdapter;
import kinon.canteen.bean.HomeBean;
import kinon.canteen.bean.UserBean;
import kinon.canteen.bean.base.BaseBean;
import kinon.canteen.fragment.BaseFragment;
import kinon.canteen.network.HttpManage;
import kinon.canteen.network.http.CommonSubscriber;
import kinon.canteen.network.http.StrCallback;
import kinon.canteen.util.HomeManage.HomeManage;
import kinon.canteen.util.RxBus;
import kinon.canteen.util.RxUtil;
import kinon.canteen.util.ShardPManager;
import kinon.canteen.util.glide.GlideUtils;
import kinon.canteen.view.DividerGridItemDecoration;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by luohao on 2017-07-05.
 */

public class PersonalFragment extends BaseFragment implements View.OnClickListener{

    private View mView;
    private RelativeLayout rl_topbar;
    private TextView tv_title;
    private TextView tv_topMsg;
    private RecyclerView recy_personal;
    private CircleImageView img_head;
    private TextView tv_user;
    private TextView tv_phone;
    private TextView tv_balance;
    private TextView tv_intefral;
    private ImageView img_enter;



    private ArrayList<HomeBean.Home> userList;
    private PersonalIcomAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_personal_layout, container, false);
        return mView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        tv_title= (TextView) mView.findViewById(R.id.tv_title);
        tv_topMsg= (TextView) mView.findViewById(R.id.tv_topMsg);
        recy_personal= (RecyclerView) mView.findViewById(R.id.recy_personal);
        img_head= (CircleImageView) mView.findViewById(R.id.img_head);
        tv_user= (TextView) mView.findViewById(R.id.tv_user);
        tv_phone= (TextView) mView.findViewById(R.id.tv_phone);
        tv_balance= (TextView) mView.findViewById(R.id.tv_balance);
        tv_intefral= (TextView) mView.findViewById(R.id.tv_intefral);
        img_enter= (ImageView) mView.findViewById(R.id.img_enter);
        rl_topbar= (RelativeLayout)mView.findViewById(R.id.rl_topbar);
        rl_topbar.setBackgroundResource(R.color.red_63);
        tv_title.setTextColor(getResources().getColor(R.color.white));
        tv_title.setText("个人中心");
        tv_topMsg.setVisibility(View.VISIBLE);

        userList=new ArrayList<>();
        getUserIcom();
        getUserInfo();
//        setRecyManager();
        addListener();


    }

    private void addListener() {
        tv_topMsg.setOnClickListener(this);
    }

    private void showUserInfo(UserBean user){
        GlideUtils.displayImage(getActivity(),user.getLogo(),img_head);
        tv_user.setText(user.getUsername());
        tv_phone.setText(user.getName());
        tv_balance.setText(user.getBalance());
        tv_intefral.setText(user.getScore());
    }

    private void setRecyManager(){
        adapter=new
                PersonalIcomAdapter(userList,getActivity());
        recy_personal.setAdapter(adapter);
         GridLayoutManager gridManager=new
                GridLayoutManager(getActivity(),3);
//        gridManager.setSpanSizeLookup(new
//                       GridLayoutManager.SpanSizeLookup() {
//            @Override
//            public int getSpanSize(int position) {
//                return 3;
//            }
//        });
        recy_personal.setLayoutManager(gridManager);
//        recy_personal.addItemDecoration(new DividerGridItemDecoration());
        adapter.notifyDataSetChanged();
    }

    private void getUserIcom(){
        Log.e("lh", "getUserIcom ==");
        setUserAdapter(HomeManage.getInstance().getHomeBean().getUser());

    }

    private void setUserAdapter(ArrayList<HomeBean.Home>list){
        Log.e("lh", "list =="+list.size() );

        userList.addAll(list);
        setRecyManager();

    }

    private void getUserInfo(){
        Call<String> call=HttpManage.getInstance()
                .getUserinfo(ShardPManager.getInstance().getToken());
        call.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String data) {
                Gson gson=new Gson();
                UserBean user=gson.fromJson(data,UserBean.class);
                showUserInfo(user);
            }

            @Override
            public void onError(int code, String error) {

            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_topMsg:
                Intent intent=new Intent(getActivity(), MessageActivity.class);
                startActivity(intent);
                break;
        }
    }
}
