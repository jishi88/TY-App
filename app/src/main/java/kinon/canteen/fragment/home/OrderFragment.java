package kinon.canteen.fragment.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.reactivex.disposables.Disposable;
import kinon.canteen.R;
import kinon.canteen.activity.OrderDetailActivity;
import kinon.canteen.adapter.OrderAdapter;
import kinon.canteen.bean.base.BaseBean;
import kinon.canteen.bean.order.OrderBean;
import kinon.canteen.fragment.BaseFragment;
import kinon.canteen.network.HttpManage;
import kinon.canteen.network.http.CommonSubscriber;
import kinon.canteen.network.http.StrCallback;
import kinon.canteen.util.RxUtil;
import kinon.canteen.util.ShardPManager;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by luohao on 2017-07-05.
 * 订单页面
 */

public class OrderFragment extends BaseFragment {

    private View mView;
    private RelativeLayout rl_topbar;
    private TextView mtv_title;
    private SmartRefreshLayout smart_order;
    private ListView mlist_order;
    private int page=10;
    private int offset=0;
    private ArrayList<OrderBean> orderList=new ArrayList<OrderBean>();
    private OrderAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_order_layout, container, false);
        return mView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        mtv_title= (TextView) mView.findViewById(R.id.tv_title);
        smart_order= (SmartRefreshLayout) mView.findViewById(R.id.smart_order);
        mlist_order= (ListView) mView.findViewById(R.id.list_order);

        mtv_title.setText(R.string.order);
        adapter=new OrderAdapter(orderList,getActivity());
        mlist_order.setAdapter(adapter);

        addListener();
        getOrderInfo();
    }
    private void addListener(){
        setSmartRefreshLayout();
        mlist_order.setOnItemClickListener(itemListener);
    }

    private AdapterView.OnItemClickListener itemListener=new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Intent intent=new Intent(getActivity(),
                    OrderDetailActivity.class);
            intent.putExtra("orderid", orderList.get(i).getId());
            startActivity(intent);
        }
    };

    private void getOrderInfo(){
        Call<String> call=HttpManage.getInstance()
                .getMyOrder(ShardPManager.getInstance().getToken(),page,offset);
        call.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String data) {
                try {
                    Gson gson=new Gson();
                    JSONArray array=new JSONArray(data);
                    for(int i=0;i<array.length();i++){
                        OrderBean order=gson.fromJson(array.get(i).toString(),OrderBean.class);
                        orderList.add(order);
                    }
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(int code, String error) {
              mToast(error);
            }
        });
    }

    private void setSmartRefreshLayout(){
        smart_order.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                refreshlayout.finishRefresh(2*1000);
                orderList.clear();
                offset=0;
                getOrderInfo();
            }
        });
        smart_order.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                refreshlayout.finishLoadmore(2*1000);
                offset += page;
                getOrderInfo();
            }
        });
    }


}
