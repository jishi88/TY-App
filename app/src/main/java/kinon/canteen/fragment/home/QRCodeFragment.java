package kinon.canteen.fragment.home;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import kinon.canteen.R;
import kinon.canteen.fragment.BaseFragment;
import kinon.canteen.network.HttpManage;
import kinon.canteen.network.http.StrCallback;
import kinon.canteen.util.QRCodeUtil;
import kinon.canteen.util.ShardPManager;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by luohao on 2017-07-05.
 */

public class QRCodeFragment extends BaseFragment {

    private View mView;
    private RelativeLayout rl_topbar;
    private TextView tv_title;
    private ImageView img_oneCode;
    private TextView tv_oneCode;
    private ImageView img_qrcode;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_qrcode_layout, container, false);
        return mView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden){
            getCodeInfo();
        }
    }

    private void initView() {

        img_oneCode= (ImageView) mView.findViewById(R.id.img_oneCode);
        tv_oneCode= (TextView) mView.findViewById(R.id.tv_oneCode);
        img_qrcode= (ImageView) mView.findViewById(R.id.img_qrcode);
        tv_title= (TextView) mView.findViewById(R.id.tv_title);
        rl_topbar= (RelativeLayout)mView.findViewById(R.id.rl_topbar);
        rl_topbar.setBackgroundResource(R.color.purple_9ea1c5);
        tv_title.setText("二维码");
        tv_title.setTextColor(getResources().getColor(R.color.white));
//        Bitmap oneCode


        getCodeInfo();

    }
    private void getCodeInfo(){
        Call<String> call= HttpManage.getInstance().getCode
                (ShardPManager.getInstance().getToken());
        call.enqueue(new StrCallback<String>() {
            @Override
            public void onSuccess(Response<String> response, String data) {
                try {
                    JSONObject object=new JSONObject(data);
                    String no=object.getString("no");
                    tv_oneCode.setText(no);
                    img_oneCode.setImageBitmap(QRCodeUtil.createOneCode(260,70,no));
                    img_qrcode.setImageBitmap(QRCodeUtil.createQRCode(230,230,no));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int code, String error) {
             mToast(error);
            }
        });
    }


}
