package kinon.canteen.fragment;

import android.support.v4.app.Fragment;
import android.widget.Toast;

import kinon.canteen.application.MyApplication;

/**
 * Created by luohao on 2017-06-30.
 *
 */

public class BaseFragment extends Fragment {

    protected void mToast(String msg){
        Toast.makeText(MyApplication.getInstance(),msg,Toast.LENGTH_SHORT).show();
    }



}
