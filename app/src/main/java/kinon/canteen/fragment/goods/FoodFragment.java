package kinon.canteen.fragment.goods;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;

import io.reactivex.disposables.Disposable;
import kinon.canteen.R;
import kinon.canteen.adapter.FoodAdapter;
import kinon.canteen.bean.ShopCartBean;
import kinon.canteen.bean.UpCartBean;
import kinon.canteen.bean.GoodsBean;
import kinon.canteen.bean.base.BaseBean;
import kinon.canteen.fragment.BaseFragment;
import kinon.canteen.network.HttpManage;
import kinon.canteen.network.http.CommonSubscriber;
import kinon.canteen.util.MyDecimalFormat;
import kinon.canteen.util.RxUtil;
import kinon.canteen.util.ShardPManager;

/**
 * Created by lhqq on 2018-01-03.
 */

public class FoodFragment extends BaseFragment {

    private View mView;
    private SmartRefreshLayout smart_good;
    private ListView list_goods;
    private int page=30;
    private int offset=0;
    private int  catid;
    private ArrayList<GoodsBean> goodList;
    private FoodAdapter adapter;
    private int mGoodTpye;



    public FoodFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView=inflater.inflate(R.layout.item_fragment_good,container,false);
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        smart_good= (SmartRefreshLayout) mView.findViewById(R.id.smart_good);
        list_goods= (ListView) mView.findViewById(R.id.list_goods);
        goodList=new ArrayList<>();
        Bundle bundle=getArguments();
         catid=bundle.getInt("catid");
        mGoodTpye=bundle.getInt("goodType");
        setSmartRefreshLayout();
        adapter=new FoodAdapter(goodList,getActivity(),mGoodTpye);
        list_goods.setAdapter(adapter);
        getGoods();

    }

    private void getGoods(){
        Disposable di= HttpManage.getInstance()
                .getProducts(ShardPManager.getInstance().getToken(),
                        catid,page,offset)
                .compose(RxUtil.<BaseBean<ArrayList<GoodsBean>>>rxSchedulerHelper())
                .compose(RxUtil.<ArrayList<GoodsBean>>handleResult())
                .subscribeWith(new CommonSubscriber<ArrayList<GoodsBean>>() {
                    @Override
                    public void onNext(ArrayList<GoodsBean> goodList) {
                        showData(goodList);
                    }

                    @Override
                    public void onFail(Throwable t, int code, String msg) {

                    }
                });
    }

    private void showData(ArrayList<GoodsBean> list){
        goodList.addAll(list);
        adapter.notifyDataSetChanged();
    }
    private FoodAdapter.onAdapterClickListener adapterListener=new FoodAdapter.onAdapterClickListener() {
        @Override
        public void onAdapterListener(int position, UpCartBean bean) {

        }
    };


    private void setSmartRefreshLayout(){
        smart_good.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                refreshlayout.finishRefresh(2*1000);
                goodList.clear();
                offset=0;
                getGoods();
            }
        });
        smart_good.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                refreshlayout.finishLoadmore(2*1000);
                offset += page;
                getGoods();
            }
        });
    }

}
