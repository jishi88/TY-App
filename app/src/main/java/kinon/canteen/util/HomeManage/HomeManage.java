package kinon.canteen.util.HomeManage;

import kinon.canteen.bean.HomeBean;

/**
 * Created by lhqq on 2018-01-04.
 */

public class HomeManage {

    private static HomeManage homeManage=null;
    private HomeBean homeBean=null;


    public static HomeManage getInstance() {
        if (homeManage == null) {
            synchronized (HomeManage.class) {
                if (homeManage == null) {
                    homeManage = new HomeManage();
                }
                return homeManage;
            }
        } else {
            return homeManage;
        }
    }



    public HomeBean getHomeBean() {
        return homeBean;
    }

    public void setHomeBean(HomeBean homeBean) {
        this.homeBean = homeBean;
    }
}
