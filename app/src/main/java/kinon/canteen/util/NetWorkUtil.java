package kinon.canteen.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetWorkUtil {
	public static boolean isNetwork(Context context) {
		// Context context = activity.getApplicationContext();
		// 获取手机所有连接管理对象（包括对wi-fi,net等连接的管理）
		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivityManager == null) {
			return false;
		} else {
			// // 获取NetworkInfo对象
			// NetworkInfo[] networkInfo =
			// connectivityManager.getAllNetworkInfo();
			// if (networkInfo != null && networkInfo.length > 0)
			// {
			// for (int i = 0; i < networkInfo.length; i++)
			// {
			// // 判断当前网络状态是否为连接状态
			// if (networkInfo[i].getState() == NetworkInfo.State.CONNECTED)
			// {
			// return true;
			// }
			// }
			// }

			NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
			if(networkInfo!=null && networkInfo.isConnected())
			{
				//当前网络是链接的
				if(networkInfo.getState()== NetworkInfo.State.CONNECTED)
				{
					//当前网络可用
					return true;
				}
				
			}
		}

		return false;

	}

}
