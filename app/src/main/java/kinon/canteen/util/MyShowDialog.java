package kinon.canteen.util;
import android.content.Context;
import android.os.Handler;

import kinon.canteen.R;
import kinon.canteen.view.dialog.LoadingDialog;

public class MyShowDialog {
	// 加载使用的dialog
		public static LoadingDialog loadingDialog;
	public static void showLoadingDialog(Context context, String paramString){
		loadingDialog = new LoadingDialog(context, R.style.ql_loading_dialog,
				R.layout.custom_loading_layout);
		loadingDialog.setCanceledOnTouchOutside(true);
		loadingDialog.setCancelable(true);
		loadingDialog.show();
		loadingDialog.setText(paramString);
	}
	public static void showLoadingDialog(Context context){
		showLoadingDialog(context,null);
	}
	/**
	 * 关闭加载的图标
	 */
	public static void closeLoadingDialog() {
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				try {
					if (loadingDialog != null) {
						loadingDialog.dismiss();
						loadingDialog = null;
					}
				} catch (Exception e){
					
				}
			}
		}, 100);
	}
}
