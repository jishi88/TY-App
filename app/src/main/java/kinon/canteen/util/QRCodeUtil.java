package kinon.canteen.util;

import android.graphics.Bitmap;
import android.text.TextUtils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeReader;
import com.google.zxing.qrcode.QRCodeWriter;

import java.util.Hashtable;

/**
 * Created by lhqq on 2017-11-30.
 */

public class QRCodeUtil {

    public static Bitmap createQRCode(int width,int height,String content){
        if(TextUtils.isEmpty(content)){
            return null;
        }
//        Hashtable
//        BitMatrix bMatrix=new QRCodeWriter().encode(content, BarcodeFormat.QR_CODE);
        try {
            // 生成二维矩阵,编码时指定大小,不要生成了图片以后再进行缩放,这样会模糊导致识别失败
            BitMatrix bMatrix=new MultiFormatWriter()
                    .encode(content,BarcodeFormat.QR_CODE,width,height);
            // 二维矩阵转为一维像素数组,也就是一直横着排了
            int [] pixels=new int[width*height];
            for (int y=0;y<height;y++){
                for(int x=0;x<width;x++){
                    if(bMatrix.get(x,y)){
                        pixels[y*width+x]=0xff000000;
                    }else{
                        pixels[y * width + x] = 0xffffffff;
                    }
                }
            }
            Bitmap bitmap=Bitmap.createBitmap(width,height,
                    Bitmap.Config.ARGB_8888);
            bitmap.setPixels(pixels,0,width,0,0,width,height);
            return bitmap;
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static  Bitmap createOneCode(int width,int height,String content){
        if(TextUtils.isEmpty(content)){
            return null;
        }
        try {
            BitMatrix bitMatrix=new MultiFormatWriter()
                    .encode(content,BarcodeFormat.CODE_128,width,height);
            int [] pixels=new int[width*height];
            for (int y=0;y<height;y++){
                for(int x=0;x<width;x++){
                    if(bitMatrix.get(x,y)){
                        pixels[y*width+x]=0xff000000;
                    }else{
                        pixels[y * width + x] = 0xffffffff;
                    }
                }
            }
            Bitmap bitmap=Bitmap.createBitmap(width,height,
                    Bitmap.Config.ARGB_8888);
            bitmap.setPixels(pixels,0,width,0,0,width,height);
            return bitmap;
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return null;
    }



}
