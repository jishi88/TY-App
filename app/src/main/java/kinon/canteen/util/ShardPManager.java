package kinon.canteen.util;

import android.content.Context;
import android.content.SharedPreferences;

import kinon.canteen.application.MyApplication;

/**
 * Created by lhqq on 2017-12-26.
 */

public class ShardPManager {

    private static ShardPManager mShardPManager=null;
    private static SharedPreferences mSp=null;
    private static SharedPreferences.Editor mEditor=null;

    private final String CANTEEN_APP="canteen_app";


    public static ShardPManager getInstance(){
        if(mShardPManager==null || mSp==null || mEditor==null){
            mShardPManager=new ShardPManager();
        }

        return mShardPManager;
    }

    private ShardPManager(){
        mSp= MyApplication.getInstance().getSharedPreferences(CANTEEN_APP,
                Context.MODE_APPEND);
        mEditor=mSp.edit();
    }

    public String getToken(){
        return mSp.getString("token","");
    }

    public void putStr(String key,String value){
        mEditor.putString(key,value);
    }
    public String getStr(String key){
        return mSp.getString(key,"");
    }

    public void putStrCommit(String key,String value){
        mEditor.putString(key,value);
        commit();
    }


    public void commit(){
        mEditor.commit();
    }



}
