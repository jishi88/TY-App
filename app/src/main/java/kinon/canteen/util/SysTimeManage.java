package kinon.canteen.util;

import android.text.format.DateFormat;
import android.util.Log;

/**
 * Created by luohao on 2017-07-25.
 */

public class SysTimeManage {

    public static SysTimeManage timeManage=null;

    public static  SysTimeManage getInstage(){
       if(timeManage==null){
           timeManage=new SysTimeManage();
       }
       return timeManage;
    }

    public String getDateTime(long sysTime){
        CharSequence timeStr= DateFormat.format("yyyy-MM-dd HH:mm:ss", sysTime);
        return timeStr.toString();
    }

    public String getDate(long sysTime){
        CharSequence timeStr= DateFormat.format("yyyy-MM-dd", sysTime);
        return timeStr.toString();
    }

    public String getOrderId(long sysTime){
//        CharSequence timeStr= DateFormat.format("yyyyMMddHHmmss", sysTime);
        CharSequence timeStr= DateFormat.format("yyyyMMddHH", sysTime);
        return timeStr+ String.valueOf(sysTime).substring(7);
    }

    public String getYMD(long sysTime){
        CharSequence timeStr= DateFormat.format("yyyyMMdd", sysTime);
        return timeStr+ String.valueOf(sysTime);
    }
//    public int getTimeHour(){
//        Time t=new Time();
//        int hour = t.hour;
//    }



}
