package kinon.canteen.util;



import android.util.Log;

import org.json.JSONObject;
import org.reactivestreams.Publisher;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.FlowableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import kinon.canteen.bean.SendCodeBean;
import kinon.canteen.bean.base.BaseBean;
import kinon.canteen.network.http.ApiException;
import kinon.canteen.network.http.MyFunction;
import rx.functions.Func1;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;


/**
 * Created by lhqq on 2017-12-19.
 */

public class RxUtil {


    /**
     * 统一线程处理
     * @param <T>
     * @return
     */

    public static <T> FlowableTransformer<T,T> rxSchedulerHelper(){
        return new FlowableTransformer<T, T>() {
            @Override
            public Flowable<T> apply(Flowable<T> observable) {
                Log.e("lh", "rxSchedulerHelper== " );
                return observable.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

            }
        };
    }

//    @Override
//    public Observable<T> call(Observable<HttpResult<T>> responseObservable) {
//        return responseObservable.map(new Func1<HttpResult<T>, T>() {
//            @Override
//            public T call(HttpResult<T> httpResult) {
//                // 通过对返回码进行业务判断决定是返回错误还是正常取数据
////                if (httpResult.getCode() != 200) throw new RuntimeException(httpResult.getMessage());
//                if (httpResult.getCode() != ErrorType.SUCCESS) throw new ServerException(httpResult.getMessage(), httpResult.getCode());
//                return httpResult.getData();
//            }
//        }).onErrorResumeNext(new Func1<Throwable, Observable<? extends T>>() {
//            @Override
//            public Observable<? extends T> call(Throwable throwable) {
//                //ExceptionEngine 为处理异常的驱动器
//                throwable.printStackTrace();
//                return Observable.error(ExceptionEngine.handleException(throwable));
//            }
//        });
//    }




    public static <T> FlowableTransformer<BaseBean<T>, T> handleResult() {   //compose判断结果
        return new FlowableTransformer<BaseBean<T>, T>() {
            @Override
            public Flowable<T> apply(final Flowable<BaseBean<T>> httpResponseFlowable) {
                Log.e("lh", "handleResult1== ");
                boolean istrue=false;
                    return httpResponseFlowable.flatMap
                            (new Function<BaseBean<T>, Flowable<T>>() {
                                @Override
                                public Flowable<T> apply(BaseBean<T> tHttpResponse) {
                                    Log.e("lh", "code== "+tHttpResponse.getCode() );
                                    if(tHttpResponse.getCode()) {
                                        return createData(tHttpResponse.getData());
                                    } else {
                                        return Flowable.error(new ApiException(tHttpResponse.getMsg()));
                                    }
                                }
                            });
            }
        };
    }

    /**
     * 生成Flowble
     * @param t
     * @param <T>
     * @return
     */
    public static <T> Flowable<T> createData(final T t) {
        return Flowable.create(new FlowableOnSubscribe<T>() {
            @Override
            public void subscribe(FlowableEmitter<T> emitter) throws Exception {
                try {
                    emitter.onNext(t);
                    emitter.onComplete();
                } catch (Exception e) {
                    emitter.onError(e);
                }
            }
        }, BackpressureStrategy.BUFFER);
    }

}
