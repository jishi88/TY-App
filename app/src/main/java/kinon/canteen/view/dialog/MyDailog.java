package kinon.canteen.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import kinon.canteen.R;

/**
 * Created by lhqq on 2018-01-05.
 */

public class MyDailog extends Dialog implements View.OnClickListener {

    private ViewGroup mView;

    public MyDailog(Context context,String msg){
        this(context,null, msg);
    }

    public MyDailog(Context context,String title,String msg){
        super(context, R.style.Base_Theme_AppCompat_Dialog);
        initDialog(context,title,msg);
    }

    private void initDialog(Context context,String title,String msg) {
        setContentView(R.layout.dialog_context_layout);
        setParams(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);

    }

    public void setParams(int width, int height)
    {
        WindowManager.LayoutParams dialogParams = this.getWindow().getAttributes();
        dialogParams.width = width;
        dialogParams.height = height;
        this.getWindow().setAttributes(dialogParams);
    }

    private ViewGroup createDialogView(Context context, int layoutId){
        mView= (ViewGroup) LayoutInflater.from(context).inflate(layoutId,null);
        return mView;
    }
    public View findChildViewById(int id)
    {
        return mView.findViewById(id);
    }

    @Override
    public void onClick(View view) {

    }
}
