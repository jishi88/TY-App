package kinon.canteen.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import kinon.canteen.R;


/**
 * Created by luohao on 2017-07-18.
 * 自定义的dialog
 */

public class CustomDialog extends Dialog implements View.OnClickListener{


    private ViewGroup mView;
    private LinearLayout ll_dialogTop;
    private TextView tv_dialogTitle;
    private TextView tv_dialogMessage;
    private TextView tv_dialogNo;
    private TextView tv_dialogYes;

    private DialogClickListener listener;


    public CustomDialog(Context context, String title, String msg,
                        String confirm, String cancel, DialogClickListener confirmListener){

        this(context, title, msg, confirm, cancel,0,confirmListener);
    }

    public CustomDialog(Context context, String msg, DialogClickListener confirmListener){

        this(context, null, msg, null, null,0,confirmListener);
    }

    public CustomDialog(Context context, String title, String msg, String confirm, String cancel,
                        int msgSize,
                        DialogClickListener listener) {
        super(context,R.style.ql_loading_dialog);
        this.listener=listener;
        initDialog(context,title,msg,confirm,cancel);
    }

    private void initDialog(Context context, String title, String message,
                            String confirm, String cancel ){
        setContentView(createDialogView(context, R.layout.dialog_context_layout));
        setParams(800, LayoutParams.WRAP_CONTENT);
        ll_dialogTop= (LinearLayout) mView.findViewById(R.id.ll_dialogTop);
        tv_dialogTitle= (TextView) mView.findViewById(R.id.tv_dialogTitle);
        tv_dialogMessage= (TextView) mView.findViewById(R.id.tv_dialogMessage);
        tv_dialogNo= (TextView) mView.findViewById(R.id.tv_dialogNo);
        tv_dialogYes= (TextView) mView.findViewById(R.id.tv_dialogYes);
        tv_dialogNo.setOnClickListener(this);
        tv_dialogYes.setOnClickListener(this);
        //判断标题是否为空
        if(TextUtils.isEmpty(title)){
            ll_dialogTop.setVisibility(View.GONE);
        }else{
            tv_dialogTitle.setText(title);
        }
        if(!TextUtils.isEmpty(message)){
            tv_dialogMessage.setText(message);
        }

        if(!TextUtils.isEmpty(cancel)){
            tv_dialogNo.setText(cancel);
        }
        if(!TextUtils.isEmpty(confirm)){
            tv_dialogYes.setText(confirm);
        }

    }


    @Override
    public void onClick(View view) {
        if(listener==null){
            dismiss();
            Log.e("", "listener==null");
            return;
        }
        switch (view.getId()){
            case R.id.tv_dialogNo:
                listener.onDialogClick(0);
                 dismiss();

            break;
            case R.id.tv_dialogYes:
                listener.onDialogClick(1);
                dismiss();
                break;
        }
    }

    public void setParams(int width, int height)
    {
        WindowManager.LayoutParams dialogParams = this.getWindow().getAttributes();
        dialogParams.width = width;
        dialogParams.height = height;
        this.getWindow().setAttributes(dialogParams);
    }

    /**
     * 找到布局
     * @param layoutId 布局id
     * @return
     */
    private ViewGroup createDialogView(Context context, int layoutId){
        mView= (ViewGroup) LayoutInflater.from(context).inflate(layoutId,null);
        return mView;
    }
    public View findChildViewById(int id)
    {
        return mView.findViewById(id);
    }

    public interface DialogClickListener
    {
        public void onDialogClick(int btn);
    }

    public void setDialogClickListener(DialogClickListener listener){
        this.listener=listener;
    }


}
