package kinon.canteen.view.dialog;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import kinon.canteen.R;


/**
 * 
 * @author luohao 
 * @时间：2017-1-12.下午3:53:17
 * @类名：LoadingDialog.java
 * @功能：开始加载
 */
public class LoadingDialog extends Dialog {
	private int layout;
	private TextView message;
	

	public LoadingDialog(Context context, int theme, int layout) {
		super(context, theme);
		this.layout=layout;
	}
	protected void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		setContentView(this.layout);
		this.message = ((TextView) findViewById(R.id.message_loading));
	}
	
	public void setText(String paramString) {
		this.message.setText(paramString);
	}

}
